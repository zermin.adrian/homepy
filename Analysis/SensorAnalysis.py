import matplotlib.pyplot as plt
from datetime import datetime
import pathlib


def plot_security_reward_history(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(27)
    plt.plot(history)
    plt.ylabel("Reward")
    plt.savefig(my_path + "/analysis_plots/security_reward/" + dt_string + ".png", bbox_inches='tight')


def plot_total_camera_area(history):
    plt.plot(history)
    plt.ylabel("total camera sensor area")
    plt.show()


def plot_total_motion_sensor_area(history):
    plt.plot(history)
    plt.ylabel("total motion sensor area")
    plt.show()


def plot_avg_motion_sensor_area(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(26)
    plt.plot(history)
    plt.ylabel("AVG motion sensor_area")
    plt.savefig(my_path + "/analysis_plots/avg_motion_sensor_area/" + dt_string + ".png", bbox_inches='tight')


def plot_avg_camera_sensor_area(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(24)
    plt.plot(history)
    plt.ylabel("AVG camera sensor_area")
    plt.savefig(my_path + "/analysis_plots/avg_camera_sensor_area/" + dt_string + ".png", bbox_inches='tight')
