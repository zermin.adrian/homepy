import matplotlib.pyplot as plt
from datetime import datetime
import pathlib


def plot_humidity_history(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(31)
    plt.plot(history)
    plt.ylabel("Humidity")
    plt.savefig(my_path + "/analysis_plots/humidity_history/" + dt_string + ".png", bbox_inches='tight')


def plot_temperature_history(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(32)
    plt.plot(history)
    plt.ylabel("Temperature")
    plt.savefig(my_path + "/analysis_plots/temperature_history/" + dt_string + ".png", bbox_inches='tight')


def plot_lux_history(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(33)
    plt.plot(history)
    plt.ylabel("LUX")
    plt.savefig(my_path + "/analysis_plots/lux_history/" + dt_string + ".png", bbox_inches='tight')