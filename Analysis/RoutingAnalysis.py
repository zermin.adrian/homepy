import matplotlib.pyplot as plt
from datetime import datetime
import pathlib


def plot_different_path_durations(arrived_packages):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    sources = []
    destinations = []
    hopcounts = []
    sources_and_destinations = []
    for package in arrived_packages:
        sources.append(package.source.connection_id)
        destinations.append(package.destination.connection_id)
        hopcounts.append(package.hop_count)
        if not [package.source.connection_id, package.destination.connection_id] in sources_and_destinations:
            sources_and_destinations.append([package.source.connection_id, package.destination.connection_id])
    histories = [[] for i in range(len(sources_and_destinations))]
    for source_and_destination_index in range(len(sources_and_destinations)):
        for index in range(len(sources)):
            if [sources[index], destinations[index]] == sources_and_destinations[source_and_destination_index]:
                histories[source_and_destination_index].append(hopcounts[index])
    for history_index in range(len(histories)):
        plt.figure(history_index)
        plt.plot(histories[history_index])
        plt.ylabel("hopcounts from" + str(sources_and_destinations[history_index]))
        plt.savefig(my_path + "/analysis_plots/hopcounts/" + dt_string + ".png", bbox_inches='tight')


def plot_arrived_packages(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(38)
    plt.plot(history)
    plt.ylabel("number of arrived packages per sim_duration")
    plt.savefig(my_path + "/analysis_plots/arrived_packages/" + dt_string + ".png", bbox_inches='tight')

def plot_routing_reward_history(history):
    my_path = str(pathlib.Path(__file__).parent.absolute())
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y_%H:%M:%S")
    plt.figure(35)
    plt.plot(history)
    plt.ylabel("Reward")
    plt.savefig(my_path + "/analysis_plots/routing_reward/" + dt_string + ".png", bbox_inches='tight')




