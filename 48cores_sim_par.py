from threading import Thread
from homepy.scenarios.habitat_and_routing_scenario import habitat_and_routing_scenario
from homepy.scenarios.security_and_routing_scenario import security_and_routing_scenario
from homepy.scenarios.final_BA_use_case import final_BA_use_case


def main():
    next_security_q_table = []
    next_security_states = []
    next_security_actions = []
    next_routing_q_table = []
    next_routing_states = []
    next_routing_actions = []
    next_habitat_q_table = []
    next_habitat_states = []
    next_habitat_actions = []
    device_health_q_table = []
    device_health_states = []
    device_health_actions = []
    args = (next_security_q_table, next_security_states, next_security_actions,
            next_routing_q_table, next_routing_states, next_routing_actions,
            next_habitat_q_table, next_habitat_states, next_habitat_actions,
            device_health_q_table, device_health_states, device_health_actions)

    for i in range(1):
        p = Thread(target=final_BA_use_case, args=args)
        p.start()

main()
