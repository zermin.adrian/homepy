import simpy

from homepy.simulations.Thermometer import Thermometer
from homepy.simulations.Thermostat import Thermostat
from homepy.simulations.Heater import Heater
from homepy.simulations.Habitat import Window
from homepy.simulations.Shutter import Shutter
from homepy.simulations.WindowOpener import WindowOpener
from homepy.simulations.Dehumidifier import Dehumidifier
from homepy.simulations.Humidifier import Humidifier
from homepy.simulations.AirCooler import AirCooler
from homepy.simulations.RelativeHumiditySensor import RelativeHumiditySensor
from homepy.simulations.LuminositySensor import LuminositySensor
from homepy.simulations.LightBulb import LightBulb
from homepy.controller.RoutingController import RoutingController
from homepy.controller.SecurityController import SecurityController
from homepy.resources.Inhabitant import Inhabitant
from homepy.resources.Repairman import Repairman
from homepy.simulations.Camera import Camera
from homepy.simulations.Client import Client
from homepy.simulations.Device import Device
from homepy.simulations.Gateway import Gateway
from homepy.simulations.Lock import Lock
from homepy.simulations.Habitat import Habitat
from homepy.simulations.MotionSensor import MotionSensor
from homepy.simulations.Rotator import Rotator
from homepy.simulations.Router import Router
from homepy.simulations.SecurityControl import SecurityControl, MovementPattern
from homepy.controller.DeviceHealthController import DeviceHealthController
from homepy.observer.DeviceHealthObserver import DeviceHealthObserver
from homepy.controller.HabitatController import HabitatController

from homepy.simulations.Habitat import Habitat
from homepy.observer.HabitatObserver import HabitatObserver
from homepy.observer.SecurityObserver import SecurityObserver
from homepy.observer.NetworkObserver import NetworkObserver

from Analysis.RoutingAnalysis import plot_different_path_durations, plot_arrived_packages, plot_routing_reward_history
from Analysis.HabitatAnalysis import plot_humidity_history, plot_temperature_history, plot_lux_history
from Analysis.SensorAnalysis import plot_security_reward_history

SIM_DURATION = 1000
ROUTE_DELAY = 3


def final_BA_use_case(security_q_table, security_states, security_actions,
                                  routing_q_table, routing_states, routing_actions,
                                  habitat_q_table, habitat_states, habitat_actions,
                                  device_health_q_table, devices_health_states, device_health_actions):
    habitat_epsilon = 0.2
    next_habitat_epsilon = 0
    sensor_epsilon = 0.2
    routing_epsilon = 0.2
    device_health_epsilon = 0.2
    next_security_epsilon = 0
    next_routing_epsilon = 0
    next_device_health_epsilon = 0
    avgs = []
    number_of_packages = []
    for j in range(240):
        env = simpy.Environment()
        habitat = Habitat(100, 12, 20, env)
        device_health_observer = DeviceHealthObserver(env, habitat)
        if next_routing_epsilon and next_security_epsilon:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION, device_health_observer,
                                                              next_device_health_epsilon)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            security_observer = SecurityObserver(env, habitat, device_health_controller)
            habitat_observer = HabitatObserver(env, habitat, device_health_controller)
            security_controller = SecurityController(env, security_q_table, security_states,
                                                     security_actions, SIM_DURATION, security_observer,
                                                     next_security_epsilon)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer,
                                                   next_routing_epsilon)
            habitat_controller = HabitatController(env, habitat_q_table, habitat_states,
                                                   habitat_actions, SIM_DURATION, habitat_observer,
                                                   next_habitat_epsilon, 80, 45, 18)
        else:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION,
                                                              device_health_observer,
                                                              device_health_epsilon)
            habitat_observer = HabitatObserver(env, habitat, device_health_controller)
            habitat_controller = HabitatController(env, habitat_q_table, habitat_states,
                                                   habitat_actions, SIM_DURATION, habitat_observer, habitat_epsilon, 80,
                                                   45, 18)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            security_observer = SecurityObserver(env, habitat, device_health_controller)
            security_controller = SecurityController(env, security_q_table, security_states,
                                                     security_actions, SIM_DURATION, security_observer, sensor_epsilon)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer, routing_epsilon)
        router_1 = Router(ROUTE_DELAY, 5, [22, 25], network_observer, env)
        router_2 = Router(ROUTE_DELAY, 2.4, [46, 48], network_observer, env)
        router_3 = Router(ROUTE_DELAY, 5, [47, 68], network_observer, env)
        gateway_1 = Gateway(ROUTE_DELAY, 5, [77, 48], network_observer, env)
        gateway_2 = Gateway(ROUTE_DELAY, 5, [32, 52], network_observer, env)

        client_1 = Client(gateway_2, [20, 19], network_observer, env)
        client_2 = Client(gateway_1, [20, 81], network_observer, env)
        client_3 = Client(gateway_1, [36, 72], network_observer, env)
        client_4 = Client(gateway_1, [81, 70], network_observer, env)
        client_5 = Client(gateway_2, [81, 80], network_observer, env)
        camera_1 = Camera([19, 19], "N", "TRIANGLE", 20, client_1, security_observer, env)
        camera_2 = Camera([19, 81], "N", "TRIANGLE", 20, client_2, security_observer, env)
        camera_3 = Camera([81, 21], "W", "TRIANGLE", 20, client_3, security_observer, env)
        camera_4 = Camera([81, 80], "S", "TRIANGLE", 20, client_4, security_observer, env)
        camera_5 = Camera([81, 70], "S", "TRIANGLE", 20, client_5, security_observer, env)
        dislocator_1 = Rotator([camera_1], security_observer, env)
        dislocator_2 = Rotator([camera_2], security_observer, env)
        dislocator_3 = Rotator([camera_3], security_observer, env)
        dislocator_4 = Rotator([camera_4], security_observer, env)
        dislocator_5 = Rotator([camera_5], security_observer, env)

        client_6 = Client(gateway_1, [19, 50], network_observer, env)
        client_7 = Client(gateway_1, [45, 19], network_observer, env)
        client_8 = Client(gateway_2, [45, 81], network_observer, env)
        client_9 = Client(gateway_2, [75, 81], network_observer, env)
        sensor_1 = MotionSensor([19, 50], "N", "LINE", 15, client_6, security_observer, env)
        sensor_2 = MotionSensor([45, 19], "W", "LINE", 25, client_7, security_observer, env)
        sensor_3 = MotionSensor([45, 81], "O", "LINE", 30, client_8, security_observer, env)
        sensor_4 = MotionSensor([75, 81], "O", "LINE", 30, client_9, security_observer, env)

        hallway_thermometer_client = Client(gateway_2, [36, 53], network_observer, env)
        hallway_heater = Heater(1500, [36, 53], habitat_observer, env)
        hallway_thermometer = Thermometer([36, 53], hallway_thermometer_client, habitat_observer, env)
        hallway_humidity_client = Client(gateway_1, [60, 72], network_observer, env)
        hallway_humidity_sensor = RelativeHumiditySensor([60, 72], hallway_humidity_client, habitat_observer, env)
        hallway_luminosity_client = Client(gateway_2, [50, 71], network_observer, env)
        hallway_luminosity_sensor = LuminositySensor([50, 71], hallway_luminosity_client, habitat_observer, env)
        hallway_lightbulb = LightBulb(800, 60, [40, 60], habitat_observer, env)
        hallway_thermostat = Thermostat([hallway_heater], [], habitat_observer, env, [44, 70])

        hallway_window = Window(3, [50, 79], habitat_observer, env)
        hallway_window_opener = WindowOpener(hallway_window, habitat_observer, env, [50, 79])
        hallway_shutter = Shutter([50, 79], hallway_window, habitat_observer, env)

        device_1 = Device(habitat, env, [22, 25], [router_1], security_controller, routing_controller,
                          habitat_controller,
                          device_health_observer, 3)
        device_2 = Device(habitat, env, [46, 48], [router_2], security_controller, routing_controller,
                          habitat_controller,
                          device_health_observer, 3)
        device_3 = Device(habitat, env, [47, 68], [router_3], security_controller, routing_controller,
                          habitat_controller,
                          device_health_observer, 3)
        device_4 = Device(habitat, env, [77, 48], [gateway_1], security_controller, routing_controller,
                          habitat_controller,
                          device_health_observer, 3)
        device_5 = Device(habitat, env, [32, 52], [gateway_2], security_controller, routing_controller,
                          habitat_controller,
                          device_health_observer, 3)

        device_6 = Device(habitat, env, [19, 19], [camera_1, dislocator_1, client_1], security_controller,
                          routing_controller, habitat_controller, device_health_observer, 4)
        device_7 = Device(habitat, env, [19, 81], [camera_2, dislocator_2, client_2], security_controller,
                          routing_controller, habitat_controller, device_health_observer, 4)
        device_8 = Device(habitat, env, [81, 21], [camera_3, dislocator_3, client_3], security_controller,
                          routing_controller, habitat_controller, device_health_observer, 4)
        device_9 = Device(habitat, env, [81, 80], [camera_4, dislocator_4, client_4], security_controller,
                          routing_controller, habitat_controller, device_health_observer, 4)
        device_10 = Device(habitat, env, [81, 70], [camera_5, dislocator_5, client_5], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)

        device_11 = Device(habitat, env, [19, 50], [sensor_1, client_6], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)
        device_12 = Device(habitat, env, [45, 19], [sensor_2, client_7], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)
        device_13 = Device(habitat, env, [45, 81], [sensor_3, client_8], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)
        device_14 = Device(habitat, env, [75, 81], [sensor_4, client_9], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)

        device_14 = Device(habitat, env, [36, 53], [hallway_thermometer, hallway_heater, hallway_thermometer_client],
                           security_controller, routing_controller, habitat_controller, device_health_observer, 4)
        device_15 = Device(habitat, env, [60, 72], [hallway_humidity_client, hallway_humidity_sensor],
                           security_controller, routing_controller, habitat_controller, device_health_observer, 4)
        device_16 = Device(habitat, env, [40, 60], [hallway_lightbulb],
                           security_controller, routing_controller, habitat_controller, device_health_observer, 4)
        device_17 = Device(habitat, env, [[50, 71]], [hallway_luminosity_client, hallway_luminosity_sensor],
                           security_controller, routing_controller, habitat_controller, device_health_observer, 4)
        device_18 = Device(habitat, env, [44, 70], [hallway_thermostat], security_controller,
                           routing_controller, habitat_controller, device_health_observer, 4)
        window_opener_device = Device(habitat, env, [50, 79], [hallway_window_opener, hallway_shutter], None,
                                      routing_controller, habitat_controller,
                                      device_health_observer, 7)
        env.process(hallway_window.run())
        mario = Repairman(env, "Mario", device_health_observer)
        bob = Repairman(env, "Bob", device_health_observer)
        isa = Inhabitant(env, 0, "ISA", device_health_observer)
        print("Simulation running....")
        env.process(isa.run(security_controller))
        env.process(habitat_controller.run())
        env.process(security_controller.run())
        env.process(routing_controller.run())
        env.process(device_health_controller.run())
        env.run(until=SIM_DURATION + 1)

        plot_routing_reward_history(network_observer.history.routing_reward_history)
        number_of_packages.append(len(network_observer.history.arrived_packages))
        plot_arrived_packages(number_of_packages)

        plot_temperature_history(habitat_observer.history.room_temperatures)
        plot_humidity_history(habitat_observer.history.room_humidities)
        plot_lux_history(habitat_observer.history.room_luminosities)

        plot_security_reward_history(security_observer.history.security_reward_history)
