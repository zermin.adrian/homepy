import simpy

from homepy.controller.RoutingController import RoutingController
from homepy.controller.SecurityController import SecurityController
from homepy.resources.Inhabitant import Inhabitant
from homepy.resources.Repairman import Repairman
from homepy.simulations.Camera import Camera
from homepy.simulations.Client import Client
from homepy.simulations.Device import Device
from homepy.simulations.Gateway import Gateway
from homepy.simulations.Lock import Lock
from homepy.simulations.Habitat import Habitat
from homepy.simulations.MotionSensor import MotionSensor
from homepy.simulations.Rotator import Rotator
from homepy.simulations.Router import Router
from homepy.simulations.SecurityControl import SecurityControl, MovementPattern
from homepy.controller.DeviceHealthController import DeviceHealthController
from homepy.observer.DeviceHealthObserver import DeviceHealthObserver
from homepy.observer.SecurityObserver import SecurityObserver
from homepy.observer.NetworkObserver import NetworkObserver


from Analysis.RoutingAnalysis import plot_different_path_durations, plot_arrived_packages, plot_routing_reward_history
SIM_DURATION = 1000
ROUTE_DELAY = 3


def security_and_routing_scenario(security_q_table, security_states, security_actions,
                                  routing_q_table, routing_states, routing_actions,
                                  device_health_q_table, devices_health_states, device_health_actions):
    sensor_epsilon = 0.2
    routing_epsilon = 0.2
    device_health_epsilon = 0.2
    next_security_epsilon = 0
    next_routing_epsilon = 0
    next_device_health_epsilon = 0
    avgs = []
    number_of_packages = []
    for j in range(240):
        env = simpy.Environment()
        habitat = Habitat(100, 12, 20, env)
        smart_home_observer = DeviceHealthObserver(env, habitat)
        if next_routing_epsilon and next_security_epsilon:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION, smart_home_observer,
                                                              next_device_health_epsilon)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            security_observer = SecurityObserver(env, habitat, device_health_controller)
            security_controller = SecurityController(env, security_q_table, security_states,
                                                     security_actions, SIM_DURATION, security_observer,
                                                     next_security_epsilon)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer,
                                                   next_routing_epsilon)
        else:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION,
                                                              smart_home_observer,
                                                              device_health_epsilon)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            security_observer = SecurityObserver(env, habitat, device_health_controller)
            security_controller = SecurityController(env, security_q_table, security_states,
                                                     security_actions, SIM_DURATION, security_observer, sensor_epsilon)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer, routing_epsilon)
        router_1 = Router(ROUTE_DELAY, 5, [30, 70], network_observer, env)
        router_2 = Router(ROUTE_DELAY, 2.4, [30, 30], network_observer, env)
        router_3 = Router(ROUTE_DELAY, 5, [50, 52], network_observer, env)
        router_4 = Router(ROUTE_DELAY, 5, [60, 60], network_observer, env)
        gateway_1 = Gateway(ROUTE_DELAY, 5, [40, 40], network_observer, env)
        gateway_2 = Gateway(ROUTE_DELAY, 5, [35, 36], network_observer, env)
        client_1 = Client(gateway_2, [20, 19], network_observer, env)
        client_2 = Client(gateway_1, [20, 81], network_observer, env)
        client_3 = Client(gateway_1, [36, 72], network_observer, env)
        client_4 = Client(gateway_1, [81, 70], network_observer, env)
        client_5 = Client(gateway_2, [81, 80], network_observer, env)
        sensor_1 = MotionSensor([20, 19], "N", "LINE", 15, client_1, security_observer, env)
        sensor_2 = MotionSensor([20, 81], "N", "LINE", 25, client_2, security_observer, env)
        sensor_3 = MotionSensor([36, 72], "N", "LINE", 30, client_3, security_observer, env)
        camera_1 = Camera([81, 70], "W", "TRIANGLE", 20, client_4, security_observer, env)
        camera_2 = Camera([81, 80], "N", "TRIANGLE", 20, client_5, security_observer, env)
        dislocator_1 = Rotator([sensor_3], security_observer, env)
        dislocator_2 = Rotator([camera_1], security_observer, env)
        dislocator_3 = Rotator([camera_2], security_observer, env)
        lock1 = Lock(1234, [80, 70], security_observer, env)
        pattern = MovementPattern("SW", "NE", dislocator_2, "ROTATE", env)
        control1 = SecurityControl([pattern], security_observer, [79, 69], env)
        device_1 = Device(habitat, env, [20, 19], [sensor_1, client_1], security_controller, routing_controller, None,
                          smart_home_observer, 2)
        device_2 = Device(habitat, env, [20, 81], [sensor_2, client_2], security_controller, routing_controller, None,
                          smart_home_observer, 2)
        device_3 = Device(habitat, env, [36, 72], [sensor_3, dislocator_1, client_3], security_controller,
                          routing_controller, None, smart_home_observer, 2)
        device_4 = Device(habitat, env, [81, 70], [camera_1, dislocator_2, client_4], security_controller,
                          routing_controller, None, smart_home_observer, 4)
        device_5 = Device(habitat, env, [81, 80], [camera_2, dislocator_3, client_5], security_controller,
                          routing_controller, None, smart_home_observer, 4)
        device_6 = Device(habitat, env, [80, 70], [lock1], security_controller, routing_controller, None,
                          smart_home_observer, 8)
        device_7 = Device(habitat, env, [79, 69], [control1], security_controller, routing_controller, None,
                          smart_home_observer, 6)
        device_8 = Device(habitat, env, [40, 40], [gateway_1], security_controller, routing_controller, None,
                          smart_home_observer, 3)
        device_9 = Device(habitat, env, [35, 36], [gateway_2], security_controller, routing_controller, None,
                          smart_home_observer, 3)
        device_10 = Device(habitat, env, [30, 70], [router_1], security_controller, routing_controller, None,
                           smart_home_observer, 3)
        device_11 = Device(habitat, env, [30, 30], [router_2], security_controller, routing_controller, None,
                           smart_home_observer, 3)
        device_12 = Device(habitat, env, [50, 52], [router_3], security_controller, routing_controller, None,
                           smart_home_observer, 3)
        device_13 = Device(habitat, env, [60, 60], [router_4], security_controller, routing_controller, None,
                           smart_home_observer, 3)

        mario = Repairman(env, "Mario", smart_home_observer)
        bob = Repairman(env, "Bob", smart_home_observer)
        isa = Inhabitant(env, 0, "ISA", smart_home_observer)
        print("Simulation running....")
        env.process(isa.run(security_controller))
        env.process(security_controller.run())
        env.process(routing_controller.run())
        env.process(device_health_controller.run())
        env.run(until=SIM_DURATION + 1)
        plot_routing_reward_history(network_observer.history.routing_reward_history)
        number_of_packages.append(len(network_observer.history.arrived_packages))
        plot_arrived_packages(number_of_packages)
