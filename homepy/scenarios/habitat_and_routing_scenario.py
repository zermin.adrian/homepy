import simpy

from Analysis.RoutingAnalysis import plot_arrived_packages, plot_routing_reward_history
from Analysis.HabitatAnalysis import plot_humidity_history, plot_lux_history, plot_temperature_history
from homepy.simulations.Client import Client
from homepy.simulations.Device import Device
from homepy.simulations.Gateway import Gateway
from homepy.simulations.Router import Router
from homepy.simulations.Thermometer import Thermometer
from homepy.simulations.Thermostat import Thermostat
from homepy.simulations.Heater import Heater
from homepy.simulations.Habitat import Window
from homepy.simulations.Shutter import Shutter
from homepy.simulations.WindowOpener import WindowOpener
from homepy.simulations.Dehumidifier import Dehumidifier
from homepy.simulations.Humidifier import Humidifier
from homepy.simulations.AirCooler import AirCooler
from homepy.simulations.RelativeHumiditySensor import RelativeHumiditySensor
from homepy.simulations.LuminositySensor import LuminositySensor
from homepy.simulations.LightBulb import LightBulb
from homepy.simulations.Habitat import Habitat
from homepy.observer.HabitatObserver import HabitatObserver
from homepy.observer.DeviceHealthObserver import DeviceHealthObserver
from homepy.observer.NetworkObserver import NetworkObserver

from homepy.resources.Inhabitant import Inhabitant
from homepy.resources.Repairman import Repairman
from homepy.controller.RoutingController import RoutingController
from homepy.controller.HabitatController import HabitatController
from homepy.controller.DeviceHealthController import DeviceHealthController

SIM_DURATION = 100000
ROUTE_DELAY = 3


def habitat_and_routing_scenario(security_q_table, habitat_states, habitat_actions,
                                 routing_q_table, routing_states, routing_actions,
                                 device_health_q_table, devices_health_states, device_health_actions):
    habitat_epsilon = 0.2
    routing_epsilon = 0.2
    device_health_epsilon = 0.2
    next_habitat_epsilon = 0
    next_routing_epsilon = 0
    next_device_health_epsilon = 0
    avgs = []
    number_of_packages = []
    for j in range(240):
        env = simpy.Environment()
        habitat = Habitat(100, 12, 20, env)
        smart_home_observer = DeviceHealthObserver(env, habitat)
        if next_routing_epsilon and next_habitat_epsilon:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION, smart_home_observer,
                                                              next_device_health_epsilon)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            habitat_observer = HabitatObserver(env, habitat, device_health_controller)
            habitat_controller = HabitatController(env, security_q_table, habitat_states,
                                                   habitat_actions, SIM_DURATION, habitat_observer,
                                                   next_habitat_epsilon, 80, 45, 18)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer,
                                                   next_routing_epsilon)
        else:
            device_health_controller = DeviceHealthController(env, device_health_q_table, devices_health_states,
                                                              device_health_actions, SIM_DURATION,
                                                              smart_home_observer,
                                                              device_health_epsilon)
            network_observer = NetworkObserver(env, habitat, device_health_controller)
            habitat_observer = HabitatObserver(env, habitat, device_health_controller)
            habitat_controller = HabitatController(env, security_q_table, habitat_states,
                                                   habitat_actions, SIM_DURATION, habitat_observer, habitat_epsilon, 80,
                                                   45, 18)
            routing_controller = RoutingController(env, routing_q_table, routing_states,
                                                   routing_actions, SIM_DURATION, network_observer, routing_epsilon)

        router_1 = Router(ROUTE_DELAY, 2.4, [30, 70], network_observer, env)
        router_2 = Router(ROUTE_DELAY, 2.4, [30, 30], network_observer, env)
        router_3 = Router(ROUTE_DELAY, 2.4, [50, 52], network_observer, env)
        router_4 = Router(ROUTE_DELAY, 2.4, [60, 60], network_observer, env)
        gateway_1 = Gateway(ROUTE_DELAY, 5, [40, 40], network_observer, env)
        gateway_2 = Gateway(ROUTE_DELAY, 5, [35, 36], network_observer, env)

        hallway_heater = Heater(1500, [36, 79], habitat_observer, env)
        hallway_cooler = AirCooler(10700, 700, [50, 79], habitat_observer, env)

        hallway_thermometer_client = Client(gateway_2, [44, 70], network_observer, env)
        hallway_thermometer = Thermometer([44, 70], hallway_thermometer_client, habitat_observer, env)

        hallway_thermostat = Thermostat([hallway_heater], [hallway_cooler], habitat_observer, env, [44, 70])

        hallway_window = Window(3, [50, 79], habitat_observer, env)
        hallway_window_opener = WindowOpener(hallway_window, habitat_observer, env, [50, 79])
        hallway_shutter = Shutter([50, 79], hallway_window, habitat_observer, env)

        hallway_humidifier = Humidifier(12, 16, habitat_observer, env, [36, 51])
        hallway_dehumidifier = Dehumidifier(15, 10, 370, habitat_observer, env, [36, 65])
        hallway_humidity_client = Client(gateway_1, [36, 51], network_observer, env)
        hallway_humidity_sensor = RelativeHumiditySensor([36, 51], hallway_humidity_client, habitat_observer, env)

        hallway_luminosity_client = Client(gateway_2, [50, 71], network_observer, env)
        hallway_luminosity_sensor = LuminositySensor([50, 71], hallway_luminosity_client, habitat_observer, env)

        hallway_lightbulb = LightBulb(800, 60, [40, 60], habitat_observer, env)

        thermostat_device = Device(habitat, env, [44, 70],
                                   [hallway_thermometer_client, hallway_thermometer, hallway_thermostat],
                                   None, routing_controller,
                                   habitat_controller, smart_home_observer, 7)

        humidifier_device = Device(habitat, env, [36, 51],
                                   [hallway_humidifier, hallway_humidity_client, hallway_humidity_sensor]
                                   , None, routing_controller, habitat_controller, smart_home_observer, 8)

        dehumidifier_device = Device(habitat, env, [36, 65], [hallway_dehumidifier], None, routing_controller,
                                     habitat_controller,
                                     smart_home_observer, 2)

        luminosity_device = Device(habitat, env, [50, 71], [hallway_luminosity_client, hallway_luminosity_sensor], None,
                                   routing_controller, habitat_controller, smart_home_observer, 4)

        window_opener_device = Device(habitat, env, [50, 79], [hallway_window_opener, hallway_shutter], None,
                                      routing_controller, habitat_controller,
                                      smart_home_observer, 7)

        heater_device = Device(habitat, env, [36, 79], [hallway_heater], None, routing_controller, habitat_controller,
                               smart_home_observer, 6)

        cooler_device = Device(habitat, env, [50, 79], [hallway_cooler], None, routing_controller, habitat_controller,
                               smart_home_observer, 9)

        dimmable_lightbulb = Device(habitat, env, [40, 60], [hallway_lightbulb], None, routing_controller,
                                    habitat_controller, smart_home_observer, 2)

        device_8 = Device(habitat, env, [40, 40], [gateway_1], None, routing_controller, habitat_controller,
                          smart_home_observer, 3)
        device_9 = Device(habitat, env, [35, 36], [gateway_2], None, routing_controller, habitat_controller,
                          smart_home_observer, 3)
        device_10 = Device(habitat, env, [30, 70], [router_1], None, routing_controller, habitat_controller,
                           smart_home_observer, 3)
        device_11 = Device(habitat, env, [30, 30], [router_2], None, routing_controller, habitat_controller,
                           smart_home_observer, 3)
        device_12 = Device(habitat, env, [50, 52], [router_3], None, routing_controller, habitat_controller,
                           smart_home_observer, 3)
        device_13 = Device(habitat, env, [60, 60], [router_4], None, routing_controller, habitat_controller,
                           smart_home_observer, 3)
        env.process(hallway_window.run())

        mario = Repairman(env, "Mario", smart_home_observer)
        bob = Repairman(env, "Bob", smart_home_observer)
        isa = Inhabitant(env, 0.0001, "ISA", smart_home_observer)
        print("Simulation running....")
        env.process(isa.run(habitat_controller))
        env.process(habitat_controller.run())
        env.process(routing_controller.run())
        env.process(device_health_controller.run())
        env.run(until=SIM_DURATION)
