import numpy as np
import random
from homepy.controller.Controller import Controller

"""
Controller Mechanism for the environmental regulation of a simulated smart home.
"""


class HabitatController(Controller):
    def __init__(self, env, q_table, states, actions, sim_duration, observer, epsilon,
                 lux_goal, relative_humidity_goal, temperature_goal):
        """
        Constructor for the control mechanism of environmental parameters in a simulated smart home
        Parameters
        ----------
        env
            The SimPy environment the simulations runs in
        q_table
            The Q-table on which our controller operates.
        states
            All known states of the simulated sensors.
        actions
            All discovered actions for every known state.
        sim_duration
            The simulations' duration
        observer
            The current observer of the simulations. Based on this our states get extracted.
        epsilon
            Epsilon, describing the ratio of exploration vs. exploitation currently used.
        lux_goal
            The desired lux [lm/m²] in the simulated smart home.
        relative_humidity_goal
            The desired relative humidity percentage [%] in the simulated smart home.
        temperature_goal
            The desired temperature  [°C] in the simulated smart home.
        """
        super(HabitatController, self).__init__(env, q_table, states, actions, epsilon, sim_duration, observer, 0.99995,
                                                0.9, 0.995, 0.8)
        self.window_opener_map = []
        self.cooler_map = []
        self.heater_map = []
        self.dimmer_map = []
        self.shutter_map = []
        self.dehumidifier_map = []
        self.humidifier_map = []
        self.lumen_goal = lux_goal
        self.relative_humidity_goal = relative_humidity_goal
        self.temperature_goal = temperature_goal
        self.map_size = 0

    def run(self):
        """
        The run function continuously updates the states and the correspondent actions, using a Q-Learning
        approach to reinforce "good" behaviour, based on a reward function.
        """
        while True:
            now = len(self.observer.history.habitat_state_history) - 1
            self.current_state = self.observer.history.habitat_state_history[now]
            self.map_size = len(self.current_state[0])
            self.previous_stateid = self.state_id
            self.state_id = self.map(self.current_state)
            if now >= 1:
                reward = self.compute_reward(now)
                next_action_id = np.argmax(self.q_table[self.state_id])
                self.q_table[self.previous_stateid][self.action_id] = (1 - self.alpha) * self.q_table[
                    self.previous_stateid][self.action_id] + self.alpha * (reward + self.gamma * self.q_table[
                    self.state_id][next_action_id])
            if self.epsilon >= self.epsilon_min:
                self.epsilon *= self.epsilon_decay
            if len(self.actions[self.state_id]) < 1 or random.uniform(0, 1) < self.epsilon:
                self.action = self.initialize_action(self.state_id)  # explore
                self.action_id = self.actions[self.state_id].index(self.action)
            else:
                self.action_id = np.argmax(self.q_table[self.state_id])  # exploit
                self.action = self.actions[self.state_id][self.action_id]
            self.window_opener_map = self.action[0][0]
            self.cooler_map = self.action[0][1]
            self.heater_map = self.action[0][2]
            self.dimmer_map = self.action[0][3]
            self.shutter_map = self.action[0][4]
            self.dehumidifier_map = self.action[0][5]
            self.humidifier_map = self.action[0][6]
            yield self.env.timeout(1)

    def compute_reward(self, now):
        """
        Calculates the reward of the last taken action by comparing the progress made toward the
        given environmental goals.

        Parameters
        ----------
        now
            The current time of the simulation, for accessing the history.
        Returns
        -------
        reward : int
            Numerical value describing how "good" a certain action was.
        """
        current_state = self.observer.history.habitat_state_history[now]
        past_state = self.observer.history.habitat_state_history[now - 1]

        room_temperature_differences = [1000 for i in range(len(current_state[1]))]
        room_humidity_differences = [1000 for i in range(len(current_state[1]))]
        room_luminosity_differences = [1000 for i in range(len(current_state[1]))]
        for room in current_state[1]:
            for location in room:
                habitat_tuple = current_state[0][location[0]][location[1]]
                past_tuple = past_state[0][location[0]][location[1]]
                if habitat_tuple.thermometer:
                    temperature = habitat_tuple.thermometer.current
                    past_temperature = past_tuple.thermometer.current
                    if temperature and past_temperature:
                        room_temperature_differences[current_state[1].index(room)] = \
                            abs(self.temperature_goal - past_temperature) - abs(self.temperature_goal - temperature) * len(room.coordinates)
                    elif temperature:
                        room_temperature_differences[current_state[1].index(room)] = \
                            (abs(self.temperature_goal - temperature) * -1) * len(room.coordinates)
                if habitat_tuple.luminositysensor:
                    lux = habitat_tuple.luminositysensor.current
                    past_lux = past_tuple.luminositysensor.current
                    if lux and past_lux:
                        room_luminosity_differences[current_state[1].index(room)] = \
                            (abs(self.lumen_goal - past_lux) - abs(self.lumen_goal - lux)) * len(room.coordinates)
                    elif lux:
                        room_luminosity_differences[current_state[1].index(room)] = (abs(self.lumen_goal - lux) * -1) * len(room.coordinates)
                if habitat_tuple.humiditysensor:
                    relative_humidity = habitat_tuple.humiditysensor.current
                    past_relative_humidity = past_tuple.humiditysensor.current
                    if relative_humidity and past_relative_humidity:
                        room_humidity_differences[current_state[1].index(room)] = \
                            (abs(self.relative_humidity_goal - past_relative_humidity) - abs(self.relative_humidity_goal - relative_humidity)) * len(room.coordinates)
                    elif relative_humidity:
                        (-1 * abs(self.relative_humidity_goal - relative_humidity)) * len(room.coordinates)
            return sum(room_luminosity_differences) + sum(room_humidity_differences) + sum(room_temperature_differences)

    def map(self, state):
        """
        Maps a state onto a past state.

        Parameters
        ----------
        state
            The state that has to be mapped
        Returns
        -------
        state_id :  int
            ID of the past state, used for reference in the Q-Table.
        """
        for past_state in self.states:
            if self.compare_states(past_state, state): #ToDo: Look into other implementations of compare_states,
                # especially with regard to rooms and the respective actions
                print("Habiat State:" + str(self.states.index(past_state)))
                return self.states.index(past_state)
        self.states.append(state)
        state_id = self.states.index(state)
        self.q_table.append([0])
        self.actions.append([])
        print("Habitat State ID" + str(state_id) + "was added")
        return state_id

    def initialize_action(self, state_id):
        """
        Randomly initializes a possible action for a certain state, based on the id.
        Parameters
        ----------
        state_id
            State ID of the state the action needs to be performed on.

        Returns
        -------
        action : list
            Random action that can be taken in the state with a specific ID.
        """
        action = [[self.init_window_opener_map(), self.init_cooler_map(),
                  self.init_heater_map(), self.init_dimmer_map(), self.init_shutter_map(),
                  self.init_dehumidifier_map(), self.init_humidifier_map()]]
        if action not in self.actions[state_id]:
            self.actions[state_id].append(action[:])
        if len(self.actions[state_id]) != len(self.q_table[state_id]):
            self.q_table[state_id].append(0)
        return action

    def compare_states(self, past_state, state):
        """
        Compares two states and checks for equality.
        Parameters
        ----------
        past_state
            State the system was in previously.
        state
            State the system currently is in.

        Returns
        -------
        a: bool
            A bool describing whether the two states are equal.
        """
        for room in state[1]:
            past_room_light = None
            past_room_temperature = None
            past_room_humidity = None

            room_light = None
            room_temperature = None
            room_humidity = None
            for location in room:
                past_tuple = past_state[0][location[0]][location[1]]
                tuple = state[0][location[0]][location[1]]
                if tuple.thermometer and past_tuple.thermometer:
                    past_room_temperature = past_tuple.thermometer.current
                    room_temperature = tuple.thermometer.current
                if tuple.humiditysensor and past_tuple.humiditysensor:
                    past_room_humidity = past_tuple.humiditysensor.current
                    room_humidity = tuple.humiditysensor.current
                if tuple.luminositysensor and past_tuple.luminositysensor:
                    past_room_light = past_tuple.luminositysensor.current
                    room_light = tuple.luminositysensor.current
                if not self.compare_tuples(past_tuple, tuple):
                    return False
            if not room_humidity == past_room_humidity or not room_light == past_room_light or not room_temperature == past_room_temperature:
                return False
        if not state[1] == past_state[1]:
            return False
        return True

    @staticmethod
    def compare_tuples(past, current):
        """
        Checks whether two habitat_tuples are equal
        Parameters
        ----------
        past
            Past habitat state tuple.
        current
            Recent habitat state tuple

        Returns
        -------
        a: bool
            A bool describing whether the two tuples are equal.
        """
        for index in range(len(current)):
            if current[index] and past[index]:
                if not current[index].status == past[index].status:
                    return False
                if not current[index].sub_process == past[index].sub_process:
                    return False
            elif not current[index] and past[index]:
                return False
            elif current[index] and not past[index]:
                return False
        return True

    def init_window_opener_map(self):
        """
        Initiates a matrix with instructions for simulated window openers.

        Returns
        -------
        window_opener_map : list
            A matrix with instructions for simulated window openers.
        """
        actions = [-1, 0, 1]
        window_opener_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                window_opener = self.current_state[0][x_coordinate][y_coordinate].windowopener
                if window_opener:
                    if window_opener.status:
                        rand_int = random.randint(0, 2)
                        window_opener_map[x_coordinate][y_coordinate] = actions[rand_int]
        return window_opener_map

    def init_cooler_map(self):
        """
        Initiates a matrix with instructions for simulated air coolers.

        Returns
        -------
        cooler_map : list
            A matrix with instructions for simulated air coolers.
        """
        actions = [i for i in range(11)]
        cooler_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                cooler = self.current_state[0][x_coordinate][y_coordinate].cooler
                if cooler:
                    if cooler.status:
                        if cooler.sub_process is not None:
                            cooler_map[x_coordinate][y_coordinate] = (cooler.power/cooler.max_power)
                        else:
                            rand_int = random.randint(0, 10)
                            cooler_map[x_coordinate][y_coordinate] = (actions[rand_int] / 10)
        return cooler_map

    def init_heater_map(self):
        """
        Initiates a matrix with instructions for simulated heaters.

        Returns
        -------
        heater_map : list
            A matrix with instructions for simulated heaters.
        """
        actions = [[[i/10, j] for i in range(11)] for j in range(-30, 50)]
        heater_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                heater = self.current_state[0][x_coordinate][y_coordinate].heater
                if heater:
                    if heater.status:
                        if heater.sub_process is not None:
                            heater_map[x_coordinate][y_coordinate] = \
                                [heater.power/heater.max_power, heater.current_temperature]
                        else:
                            rand_y = random.randint(0, 10)
                            rand_x = random.randint(0, 79)
                            heater_map[x_coordinate][y_coordinate] = actions[rand_x][rand_y]
        return heater_map

    def init_dimmer_map(self):
        """
        Initiates a matrix with instructions for simulated dimmable light bulbs.
        Returns
        -------
        dimmer_map : list
            A matrix with instructions for simulated dimmable light bulbs.
        """
        actions = [i for i in range(11)]
        dimmer_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                dimmable_lightbulb = self.current_state[0][x_coordinate][y_coordinate].lightbulb
                if dimmable_lightbulb:
                    if dimmable_lightbulb.status:
                        rand_int = random.randint(0, 10)
                        dimmer_map[x_coordinate][y_coordinate] = (actions[rand_int] / 10)
        return dimmer_map

    def init_shutter_map(self):
        """
        Initiates a matrix with instructions for simulated window shutters.

        Returns
        -------
        shutter_map : list
            A matrix with instructions for simulated window shutters.
        """
        actions = [i for i in range(11)]
        shutter_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                shutter = self.current_state[0][x_coordinate][y_coordinate].shutter
                if shutter:
                    if shutter.status:
                        rand_int = random.randint(0, 10)
                        shutter_map[x_coordinate][y_coordinate] = (actions[rand_int] / 10)
        return shutter_map

    def init_dehumidifier_map(self):
        """
        Initiates a matrix with instructions for simulated dehumidifiers.

        Returns
        -------
        dehumidifier_map : list
            A matrix with instructions for simulated dehumidifiers.
        """
        actions = [i for i in range(11)]
        dehumidifier_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                dehumidifier = self.current_state[0][x_coordinate][y_coordinate].dehumidifier
                if dehumidifier:
                    if dehumidifier.status:
                        rand_int = random.randint(0, 10)
                        dehumidifier_map[x_coordinate][y_coordinate] = (actions[rand_int] / 10)
        return dehumidifier_map

    def init_humidifier_map(self):
        """
        Initiates a matrix with instructions for simulated humidifiers.

        Returns
        -------
        humidifier_map : list
            A matrix with instructions for simulated humidifiers.
        """
        humidifier_map = [[0 for c in range(self.map_size)] for d in range(self.map_size)]
        for x_coordinate in range(len(self.current_state[0])):
            for y_coordinate in range(len(self.current_state[0][x_coordinate])):
                humidifier = self.current_state[0][x_coordinate][y_coordinate].humidifier
                if humidifier:
                    if humidifier.status:
                        humidifier_map[x_coordinate][y_coordinate] = random.randint(0, 1)
        return humidifier_map
