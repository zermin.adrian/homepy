import random
import simpy
import numpy as np
import collections
from homepy.controller.Controller import Controller
from homepy.simulations.Device import Device
"""
Top level controller ensuring general device health and managing device repairs.
"""


class DeviceHealthController(Controller):
    def __init__(self, env, q_table, states, actions, sim_duration, observer, epsilon):
        """
        Constructor for the device health controller.
        Parameters
        ----------
        env
            The SimPy environment the simulations runs in.
        q_table
            The learning mechanisms Q-Table.
        states
            An array of states used for id generation and mapping.
        actions
            Recorded actions for each different state.
        sim_duration
            The duration of the simulation.
        observer
            The device health observer.
        epsilon
            Epsilon, declaring the exploration vs. exploitation rate for the epsilon-greedy action selection mechanism.
        """
        super(DeviceHealthController, self).__init__(env, q_table, states, actions, epsilon, sim_duration, observer,
                                                     0.99995, 0.9, 0.995, 0.8)
        self.repair_action = []

    def run(self):
        """
        The run function continuously updates the states and the correspondent actions, using a Q-Learning
        approach to reinforce "good" behaviour, based on a reward function.
        """
        while True:
            now = len(self.observer.history.smart_home_state_history) - 1
            self.current_state = self.observer.current_state
            self.previous_state_id = self.state_id
            self.state_id = self.map(self.observer.history.smart_home_state_history[now])
            if now >= 1:
                reward = self.compute_reward()
                next_action_id = np.argmax(self.q_table[self.state_id])
                self.q_table[self.previous_state_id][self.action_id] = (1 - self.alpha) * self.q_table[
                    self.previous_state_id][self.action_id] + self.alpha * (reward + self.gamma * self.q_table[
                    self.state_id][next_action_id])
            if self.epsilon >= self.epsilon_min:
                self.epsilon *= self.epsilon_decay
            if len(self.actions[self.state_id]) < 1 or random.uniform(0, 1) < self.epsilon:
                self.action = self.initialize_action(self.state_id)  # explore
                self.action_id = self.actions[self.state_id].index(self.action)
            else:
                self.action_id = np.argmax(self.q_table[self.state_id])  # exploit
                self.action = self.actions[self.state_id][self.action_id]
            self.repair_action = self.action
            if self.repair_action[0] and self.repair_action[1]:
                self.env.process(self.repair_action[1].repair(self.repair_action[0]))
            yield self.env.timeout(1)

    def initialize_action(self, state_id):
        """
        Initializes an action the controller mechanism can take in the given state.

        Parameters
        ----------
        state_id:int
            ID of the state the action is being initialized for.
        """
        action = [self.get_random_broken_device(), self.get_available_repairman()]
        if action not in self.actions[state_id]:
            self.actions[state_id].append(action[:])
        if len(self.actions[state_id]) != len(self.q_table[state_id]):
            self.q_table[state_id].append(0)
        return action

    def map(self, state):
        """
        Maps the current state to a state id from the states table.

        Returns
        -------
        state_id : int
            The ID of the same state in the states table.
        """
        for past_state in self.states:
            if self.compare_states(past_state, state):  # Check for past equal states
                print("Device_health State:" + str(self.states.index(past_state)))
                return self.states.index(past_state)
        self.states.append(state)   # No equal states found, new state gets added to states table
        state_id = self.states.index(state)
        self.q_table.append([0])
        self.actions.append([])
        print("Device Health State ID" + str(state_id) + "was added")
        return state_id

    @staticmethod
    def compare_states(past_state, state):
        """
        Checks whether two states are equal with respect to the actions they require.
        Parameters
        ----------
        past_state
            The past recorded state
        state
            The current state
        Returns
        -------
        b:bool
            Boolean denoting whether the two states are equal.

        """
        for device_index in range(len(past_state[0])):
            past_device = past_state[0][device_index]
            device = state[0][device_index]
            if not past_device.status == device.status or not past_device.priority == device.priority:
                return False
        for repairman_index in range(len(past_state[1])):
            past_repairman = past_state[1][repairman_index]
            repairman = state[1][repairman_index]
            if not repairman.busy == past_repairman.busy or not past_repairman.time_left == repairman.time_left:
                return False
        return True

    def compute_reward(self):
        """
        Computes the reward of the last action performed.
        Returns
        -------
        reward:int
            Numerical value describing how "good" a past action was.
        """
        possible = 0
        actual = 0
        for device in self.current_state[0]:
            if device.status:
                actual += device.priority
            possible += device.priority
        self.observer.history.smart_home_reward.append(actual/possible)
        return actual/possible

    def get_random_broken_device(self):
        """
        A function that randomly selects a broken device and returns it, for it to be repaired later.
        Returns
        -------
        device:Device
            A random broken device
        """
        candidates = []
        for device in self.current_state[0]:
            if not device.status:
                candidates.append(device)
        length = len(candidates)
        if length > 0:
            return candidates[random.randint(0, length - 1)]
        else:
            return []

    def get_available_repairman(self):
        """
        Gets a random available repairman or None if no repairmen are available.
        Returns
        -------
        repairman: Repairman
            A random available repairman
        """
        for repairman in self.current_state[1]:
            if not repairman.busy:
                return repairman
        return None

    def predict_security_state(self, current_state):
        """
        Predicts and alters the current security state accordingly.
        Parameters
        ----------
        current_state
            The current security state.
        """
        state_dictionary = {
            "MOTIONSENSOR": 0,
            "CAMERA": 1,
            "DISLOCATOR": 2,
            "MOTOR": 3,
            "ROTATOR": 4,
            "DOOR": 5,
            "SECURITYCONTROL": 6,
            "ALARM": 7
        }
        if self.current_state:  # check if state is already available
            for repairman in self.current_state[1]:
                if repairman.time_left == 1:
                    for component in repairman.subject.components:
                        component.status = 1
                        location = component.location
                        component_type_index = state_dictionary.get(component.type)
                        if component_type_index is not None:
                            current_state[component_type_index][location[0]][location[1]] = component
                        #   put the repaired component back into the state

    def predict_routing_state(self):
        """
        Updates the device connections whenever a device is about to come back online.
        """
        if self.current_state:  # check if state is already available
            for repairman in self.current_state[1]:
                if repairman.time_left == 1:
                    for component in repairman.subject.components:
                        if component.type == "ROUTER" or component.type == "GATEWAY":
                            component.set_signal_strengths()  # Set the components signal strengths

    def predict_habitat_state(self, current_state):
        """
        Predicts the new habitat state whenever a device is about to be repaired.
        Parameters
        ----------
        current_state
            The current habitat state
        """
        state_dictionary = {
            "HEATER": 0,
            "COOLER": 1,
            "THERMOMETER": 2,
            "HUMIDIFIER": 3,
            "DEHUMIDIFIER": 4,
            "HUMIDITYSENSOR": 5,
            "LIGHTBULB": 6,
            "LUMINOSITYSENSOR": 7,
            "WINDOW": 8,
            "SHUTTER": 9,
            "WINDOWOPENER": 10
        }
        HabitatTuple = collections.namedtuple("habitat_tuple", "heater cooler thermometer humidifier dehumidifier"
                                                               " humiditysensor lightbulb luminositysensor "
                                                               " window shutter windowopener")
        if self.current_state:  # check if state is already available
            for repairman in self.current_state[1]:
                if repairman.time_left == 1:
                    helpful_list = [0 for i in range(11)]
                    for component in repairman.subject.components:
                        component_type_index = state_dictionary.get(component.type)
                        if component_type_index is not None:
                            helpful_list[component_type_index] = component
                            #   put the repaired component back into the state
                    current_state[0][component.location[0]][component.location[1]] = HabitatTuple._make(helpful_list)
