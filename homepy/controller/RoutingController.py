import random
import sys

import numpy as np

from homepy.controller.Controller import Controller

"""
The control mechanism for a simulated network.
"""


class RoutingController(Controller):
    def __init__(self, env, q_table, states, actions, sim_duration, observer, epsilon):
        """
        Constructor of RoutingLearning

        Parameters
        ----------
        env
            The SimPy environment of the simulations.
        q_table
            The Q-table on which our controller operates.
        states
            All known states of the simulated sensors.
        actions
            All discovered actions for every known state.
        sim_duration
            The simulations' duration
        observer
            The current observer of the simulations. Based on this our states get extracted.
        epsilon
            Epsilon, describing the ratio of exploration vs. exploitation currently used.
        """
        super(RoutingController, self).__init__(env, q_table, states, actions, epsilon, sim_duration, observer, 0.99995,
                                                0.9, 0.995, 0.8)
        self.routing_table = []  # Routing table where the next destination can be looked up

    def run(self):
        """
        The run function continuously updates the states and the correspondent actions, using a Q-Controller
        approach to reinforce "good" behaviour, based on a reward function.
        """
        while True:
            now = len(self.observer.history.routing_state_history) - 1
            self.current_state = self.observer.history.routing_state_history[now]
            self.previous_stateid = self.state_id
            self.state_id = self.map(self.current_state)
            print("Routing_state id:" + str(self.state_id))
            if now >= 1:  # Reward is based on comparing two states, so no reward calculated in the first state
                reward = self.compute_reward_arrived_packages(now)
                if self.state_id == self.previous_stateid:
                    reward = 0
                next_action_id = np.argmax(self.q_table[self.state_id])
                self.q_table[self.previous_stateid][self.action_id] = (1 - self.alpha) \
                                                                      * self.q_table[self.previous_stateid][
                                                                          self.action_id] + self.alpha \
                                                                      * (reward + self.gamma *
                                                                         self.q_table[self.state_id][next_action_id])
            if self.epsilon >= self.epsilon_min:
                self.epsilon *= self.epsilon_decay
            if len(self.actions[self.state_id]) < 1 or random.uniform(0, 1) < self.epsilon:
                self.action = self.initialize_action(self.state_id)  # explore new possible actions
                self.action_id = self.actions[self.state_id].index(self.action)
            else:
                self.action_id = np.argmax(self.q_table[self.state_id])  # exploit already known actions
                self.action = self.actions[self.state_id][self.action_id]
            self.routing_table = self.action[0]
            # update routing table and device to be repaired as action
            yield self.env.timeout(1)
            if self.env.now == self.sim_duration:
                print("Final Routing Q_Table: " + str(self.q_table))
                print("Final Routing q_table_len:" + str(len(self.q_table)))
                print("Final Routing epsilon:" + str(self.epsilon))
                print(len(self.q_table[0]))

    def initialize_action(self, state_id):
        """
        Function used to initialize a random action either when exploring new actions for a known state,
        or when first selecting an action for an unknown state.

        Parameters
        ----------
        state_id : int
            The ID/Index of the state from which the action should be taken.

        Returns
        -------
        action : list
            A randomly initialized action consisting of a routing table and a device to be repaired.

        """
        action = [self.init_routing_table()]
        if action not in self.actions[state_id]:
            self.actions[state_id].append(action)
        if len(self.actions[state_id]) != len(self.q_table[state_id]):
            self.q_table[state_id].append(0)
        return action

    def init_routing_table(self):
        """
        Function to randomly initialize a routing table, according to the current state and router connections

        Returns
        -------
        routing_table : list
                A randomly initialized routing table

        """
        size = len(self.observer.connectables)
        routing_table = [[sys.maxsize for s in range(size)] for t in range(size)]
        for i in range(size):  # to get from i to j, go to node with id in [i][j]
            for j in range(size):
                routing_table[i][j] = self.get_random_connection(i)
        return routing_table

    def get_random_connection(self, index):
        number_of_connections = len(self.current_state[0])
        router_connections = self.current_state[0]
        possible_connections = []
        for i in range(number_of_connections):
            if router_connections[index][i] != 0:
                possible_connections.append(i)
        length = len(possible_connections) - 1
        if length >= 0:
            ran = random.randint(0, length)
            return possible_connections[ran]
        else:
            return None

    def compute_reward_arrived_packages(self, time):
        """
        Computes the reward based on the number and size of the arrived packages at this point in time.
        Parameters
        ----------
        time
            The current simulated time

        Returns
        -------
        reward:int
            A numerical value of how "good" the action was
        """
        reward = 0
        current_state = self.observer.history.routing_state_history[time]
        for package in current_state[3]:
            reward += package.size
        self.observer.history.routing_reward_history.append(reward)
        return reward

    def compute_reward_with_load(self, time):
        """
        Used to compute a reward based on the predicted hop count and the load (number of other packages) on an intended
        path, across all simulated packages.


        Parameters
        ----------
        time : int
            Time of the simulations, for reference in the history, to access state


        Returns
        -------
        reward : int
            A numerical reward of the last action taken.

        """
        reward = 0
        state = self.observer.history.routing_state_history[time]
        total_number_of_packages = sum([len(i) for i in state[1]]) + sum([1 for j in state[2]])
        for index in range(len(state[2])):
            current = index
            destination = state[2][index]
            if destination < len(state[2]):
                path = self.get_optimal_path(current, destination, [])
                expected_hop_count_reward = self.get_path_reward(path, current, destination)
                load = self.get_load(path, state)
                reward += expected_hop_count_reward - (load / total_number_of_packages)
        self.observer.history.routing_reward_history.append(reward)
        return reward

    def get_path_reward(self, path, current, destination):
        """ Calculates the reward of a certain path based on the hop count and the shortest discovered paths so far.

        Parameters
        ----------
        path
            The path whose reward is being calculated
        current
            The current position of the package
        destination
            The packages' destination

        Returns
        -------
        reward : int
            The reward that is given according to how short the path is, compared to previous paths.
        """
        expected_hop_count = len(path) - 1
        reward = 0
        if destination in path:
            if expected_hop_count < self.observer.shortest_path_times[current][destination]:
                reward = 2
                self.observer.shortest_path_times[current][destination] = expected_hop_count
            elif expected_hop_count == self.observer.shortest_path_times[current][destination]:
                reward = 1
            elif expected_hop_count > self.observer.shortest_path_times[current][destination]:
                reward = 0
        else:
            reward = -1
        return reward

    def get_load(self, path, state):
        """
        Gets the load (number of packages) on the path of a package
        Parameters
        ----------
        path
            Path whose load is being calculated
        state
            The current state, where each router and its queues are listed
        Returns
        -------
        load : int
            Number of packages on a certain path
        """
        load = 0
        for step in path:
            load += len(state[1][step])  # Get the length of the queue in each router along the path
        return load

    def compute_reward(self, time):
        """
        A different reward function that computes the reward based solely on the hop count of arrived packages

        Parameters
        ----------
        time
            The current simulated time of the environment
        Returns
        -------
        reward : int
            A numerical value for how good an action was, in this case based on the hop count of every arrived package

        """
        reward = 0
        for package in self.observer.arrived_packages:
            if package.arrival_time == time:
                source_index = package.source.connection_id - 1
                dest_index = package.destination.connection_id - 1
                if package.hop_count < self.observer.shortest_path_times[source_index][dest_index]:
                    reward += 2
                    self.observer.shortest_path_times[source_index][dest_index] = package.hop_count
                elif package.hop_count == self.observer.shortest_path_times[source_index][dest_index]:
                    reward += 1
                elif package.hop_count > self.observer.shortest_path_times[source_index][dest_index]:
                    reward += 0
        self.observer.history.routing_reward_history.append(reward)
        return reward

    def get_expected_transfer_time(self, path):  # ToDo: get rid of
        """
        Function to compute how long a package is going to take to take a certain path

        Parameters
        ----------
        path
            Path for which the transfer time should be calculated

        Returns
        -------
        time : int
            The time the package is expected to take for a path

        """
        if len(path) > len(self.observer.connectables):
            return len(path)
        if len(path) == 1:
            return 0
        time = self.current_state[0][path[0]][path[1]]
        path.pop(0)
        return time + self.get_expected_transfer_time(path)

    def get_optimal_path(self, current, dest, path):  # ToDo: check for non-reachable nodes
        """
        Function to look up the path a package is going to take according to the routing table.

        Parameters
        ----------
        current
            ID of the current router where the package is
        dest
            ID of the destination of the package
        path
            Path of the package

        Returns
        -------
        path : list
            Path the package is taking according to the routing table

        """
        if current == dest or len(path) > len(self.observer.connectables):
            path.append(current)
            return path
        if current is None:
            return path
        path.append(current)
        next_hop = self.routing_table[current][dest]
        return self.get_optimal_path(next_hop, dest, path)

    def map(self, state):  # map current state to an existing state or add new one and return its id
        """
        Function to map the current state onto its state-ID.
        Parameters
        ----------
        state
            A routing state

        Returns
        -------
        state_id: int
            The index/ID of the current state also used to access the Q-Table and the actions table.

        """
        for past_state in self.states:
            if past_state[0] == state[0] and self.compare_queues(past_state[1], state[1]) and past_state[2] == state[2]:
                return self.states.index(past_state)
        self.states.append(state)
        state_id = self.states.index(state)
        self.q_table.append([0])
        self.actions.append([])
        print("Routing State ID" + str(state_id) + "was added")
        return state_id

    def compare_queues(self, queue_array1, queue_array2):
        """
        Compares an array of queues by their lengths.
        Parameters
        ----------
        queue_array1
            An array of queues in different routers.

        queue_array2
            An array of queues in different routers.

        Returns
        -------
        relative_equal : bool
            A boolean value of whether the two queue arrays are equal when comparing relative queue length.
        """
        if queue_array1 == queue_array2:
            return True
        queue_lengths_1 = [len(i) for i in queue_array1]
        queue_lengths_2 = [len(j) for j in queue_array2]
        if queue_lengths_1 and queue_lengths_2:
            min1 = self.get_smallest_non_zero(queue_lengths_1)
            min2 = self.get_smallest_non_zero(queue_lengths_2)
        else:
            return False
        compare_array_1 = []
        compare_array_2 = []
        for j in queue_lengths_1:
            if j == 0:
                compare_array_1.append(j)
            else:
                compare_array_1.append(j-min1)
        for k in queue_lengths_2:
            if k == 0:
                compare_array_2.append(k)
            else:
                compare_array_2.append(k-min2)
        return compare_array_1 == compare_array_2

    @staticmethod
    def get_smallest_non_zero(array):
        smol = sys.maxsize
        for i in array:
            if 0 < i < smol:
                smol = i
        return smol