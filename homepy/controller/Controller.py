"""
Abstract base class for all controller mechanisms.
"""


class Controller:
    def __init__(self, env, q_table, states, actions, epsilon, sim_duration, observer, epsilon_decay,
                 alpha, alpha_decay, gamma):
        """
        Parameters
        ----------
        env
            The SimPy environment the simulations runs in.
        q_table
            The learning mechanisms Q-Table.
        states
            An array of states used for id generation and mapping.
        actions
            Recorded actions for each different state.
        epsilon
            Epsilon, declaring the exploration vs. exploitation rate for the epsilon-greedy action selection mechanism.
        sim_duration
            The duration of the simulation.
        observer
            The observer counterpart to the controller.
        epsilon_decay
            Decay rate for epsilon.
        alpha
            Alpha, or learning rate for the Q_learning mechanism.
        alpha_decay
            Decay rate for alpha.
        gamma
            Gamma value used in Q-Controller mechanism, declaring the value of future vs. immediate reward.
        """
        self.observer = observer
        self.states = states
        self.q_table = q_table
        self.actions = actions
        self.env = env
        self.epsilon = epsilon
        self.epsilon_decay = epsilon_decay
        self.epsilon_min = 0.005
        self.sim_duration = sim_duration
        self.alpha = alpha
        self.alpha_decay = alpha_decay
        self.alpha_min = 0.00001
        self.gamma = gamma
        self.current_state = []
        self.previous_state_id = 0
        self.state_id = []
        self.action_id = 0
        self.action = []

    def run(self):
        pass

    def initialize_action(self, state_id):
        """
        Initializes an action the controller mechanism can take in the given state.

        Parameters
        ----------
        state_id:int
            ID of the state the action is being initialized for.
        """
        pass

    def map(self, state):
        """
        Maps the current state to a state id from the states table.
        """
        pass
