"""
Created on 01.05.2020

@author Adrian Zermin
"""

import random

import numpy as np

from homepy.controller.Controller import Controller

MAPSIZE = 100

"""
The control mechanism for a simulated security network.
"""


class SecurityController(Controller):
    def __init__(self, env, q_table, states, actions, sim_duration, observer, epsilon):
        """
        Constructor of SensorLearning

        Parameters
        ----------
        env
            The SimPy environment of the simulations.
        q_table
            The Q-table on which our controller operates.
        states
            All known states of the simulated sensors.
        actions
            All discovered actions for every known state.
        sim_duration
            The simulations' duration
        observer
            The current observer of the simulations. Based on this our states get extracted.
        epsilon
            Epsilon, describing the ratio of exploration vs. exploitation currently used.
        """
        super(SecurityController, self).__init__(env, q_table, states, actions, epsilon, sim_duration, observer, 0.9999,
                                                 0.8, 0.9995, 0.8)
        self.sensor_map = []

    def run(self):
        """
        The run process continuously updates the states and the correspondent actions, using a Q-Learning
        approach to reinforce "good" behaviour, based on a reward function.
        """
        while True:
            now = len(self.observer.history.sensor_state_history) - 1
            self.current_state = self.observer.history.sensor_state_history[now]
            self.previous_stateid = self.state_id
            self.state_id = self.map(self.current_state)
            print("SecurityStateID: " + str(self.state_id))
            if now >= 1:
                reward = self.compute_reward(now)
                next_action_id = np.argmax(self.q_table[self.state_id])
                self.q_table[self.previous_stateid][self.action_id] = (1 - self.alpha) * self.q_table[
                    self.previous_stateid][self.action_id] + self.alpha * (reward + self.gamma * self.q_table[
                    self.state_id][next_action_id])
            if self.epsilon >= self.epsilon_min:
                self.epsilon *= self.epsilon_decay
            if len(self.actions[self.state_id]) < 1 or random.uniform(0, 1) < self.epsilon:
                self.action = self.initialize_action(self.state_id)  # explore
                self.action_id = self.actions[self.state_id].index(self.action)
            else:
                self.action_id = np.argmax(self.q_table[self.state_id])  # exploit
                self.action = self.actions[self.state_id][self.action_id]
            self.sensor_map = self.action[0]
            yield self.env.timeout(1)
            if self.env.now == self.sim_duration:
                print("Final Security Q_Table: " + str(self.q_table))
                print("Final Security q_table_len:" + str(len(self.q_table)))
                print("Final Security epsilon:" + str(self.epsilon))
                print(len(self.q_table[0]))

    def initialize_action(self, state_id):
        """
        Initializes an action the controller mechanism can take in the given state.

        Parameters
        ----------
        state_id:int
            ID of the state the action is being initialized for.
        """
        action = [self.init_sensor_map()]
        if action not in self.actions[state_id]:
            self.actions[state_id].append(action[:])
        if len(self.actions[state_id]) != len(self.q_table[state_id]):
            self.q_table[state_id].append(0)
        return action

    def init_sensor_map(self):
        """
        Initializes a map of directions and locations for the sensors to move to/turn into.
        Returns
        -------
        senor_map:list
            A map of directions and locations for the actuators to read.
        """
        now = len(self.observer.history.sensor_state_history) - 1
        current_state = self.observer.history.sensor_state_history[now]
        locations = [[0 for c in range(MAPSIZE)] for d in range(MAPSIZE)]
        directions = [[0 for a in range(MAPSIZE)] for b in range(MAPSIZE)]
        dislocators = current_state[2]
        motors = current_state[3]
        rotators = current_state[4]
        for x in range(len(dislocators)):
            for y in range(len(dislocators[x])):
                if dislocators[x][y]:
                    if dislocators[x][y].status and dislocators[x][y].operation_mode == "AUTO":
                        locations[x][y] = self.get_random_location(dislocators[x][y].location, locations)
                        directions[x][y] = self.get_random_direction(dislocators[x][y].direction)
                if rotators[x][y]:
                    if rotators[x][y].status and rotators[x][y].operation_mode == "AUTO":
                        if not rotators[x][y].direction:
                            print("found ya")
                        directions[x][y] = self.get_random_direction(rotators[x][y].direction)
                        locations[x][y] = 0
                if motors[x][y]:
                    if motors[x][y].status and rotators[x][y].operation_mode == "AUTO":
                        locations[x][y] = self.get_random_location(motors[x][y].location, locations)
                        directions[x][y] = 0
        sensor_map = [locations, directions]
        return sensor_map

    def get_random_location(self, location, locations):
        """
        Gets a random possible location for a component to move to.
        Parameters
        ----------
        location
            The current location of the component.
        locations
            A map of locations.

        Returns
        -------
        location:list
            A list with [x_coordinate, y_coordinate] one space from the previous location.
        """
        to_be_deleted = []
        possible_locations = [[location[0], location[1]], [location[0], location[1] + 1],
                              [location[0] + 1, location[1] + 1], [location[0] + 1, location[1]],
                              [location[0] + 1, location[1] - 1], [location[0], location[1] - 1],
                              [location[0] - 1, location[1] - 1], [location[0] - 1, location[1]],
                              [location[0] - 1, location[1] + 1]]
        next_locations = []
        for i in locations:
            for j in i:
                if j:
                    next_locations.append(i)
        for a in possible_locations:
            if a[0] >= 100 or a[1] >= 100 or a[0] < 0 or a[1] < 0 \
                    or self.observer.map[a[0]][a[1]] == 1 or a in next_locations:
                to_be_deleted.append(a)
        for d in to_be_deleted:
            possible_locations.remove(d)
        ran = random.randint(0, len(possible_locations) - 1)
        return possible_locations[ran]

    @staticmethod
    def get_random_direction(current_direction):
        """
        Returns a random direction, based on a current direction.
        Parameters
        ----------
        current_direction
            The current direction the sensor is pointing in.

        Returns
        -------
        direction:str
            A random reachable direction based on the previous direction.

        """
        dictionary = {
            "N": ["N", "NW", "NE"],
            "NE": ["N", "E", "NE"],
            "E": ["SE", "E", "NE"],
            "SE": ["E", "SE", "S"],
            "S": ["SW", "SE", "S"],
            "SW": ["SW", "W", "S"],
            "W": ["SW", "W", "NW"],
            "NW": ["NW", "W", "N"],
        }
        ran = random.randint(0, 2)
        possible_directions = dictionary.get(current_direction)
        return possible_directions[ran]

    def map(self, state):
        """
        Maps the current state to a state id from the states table.
        Parameters
        ----------
        state
            The state that has to be mapped.

        Returns
        -------
        state_id: int
            ID of the past state, used for reference in the Q-Table.
        """
        for historic_state in self.states:
            if self.compare_states(historic_state, state):
                state_id = self.states.index(historic_state)
                return state_id
        self.states.append(state[:])
        state_id = len(self.states) - 1
        self.q_table.append([0])
        self.actions.append([])
        print("State ID" + str(state_id) + "was added")
        return state_id

    def compute_reward(self, history_id):
        motion_sensor_area_old = self.compute_coverage(
            self.observer.history.sensor_state_history[history_id - 1][0])
        camera_area_old = self.compute_coverage(self.observer.history.sensor_state_history[history_id - 1][1])
        motion_sensor_area_new = self.compute_coverage(self.observer.history.sensor_state_history[history_id][0])
        camera_area_new = self.compute_coverage(self.observer.history.sensor_state_history[history_id][1])
        self.observer.history.total_camera_sensor_area.append(np.sum(camera_area_new))
        self.observer.history.total_motion_sensor_area.append(np.sum(motion_sensor_area_new))
        reward = self.get_weighted_reward(motion_sensor_area_old, motion_sensor_area_new)
        reward += self.get_weighted_reward(camera_area_old, camera_area_new)
        self.observer.history.security_reward_history.append(reward)
        return reward

    @staticmethod
    def get_weighted_reward(coverage_old, coverage_new):
        reward = 0
        for i in range(len(coverage_new)):
            for j in range(len(coverage_new[i])):
                if coverage_new[i][j] and coverage_old[i][j]:
                    reward += 1
                elif coverage_new[i][j] and not coverage_old[i][j]:
                    reward += 2
        return reward

    @staticmethod
    def compute_coverage(sensor_array):
        coverage_matrix = np.array([[0 for a in range(MAPSIZE)] for b in range(MAPSIZE)])
        for i in sensor_array:
            for j in i:
                if j:
                    for location in j.sensor_area:
                        coverage_matrix[location[0]][location[1]] = 1
        return coverage_matrix

    def compare_states(self, past_state, present_state):
        compare_dict = {
            0: self.compare_sensor_map(past_state[0], present_state[0]),
            1: self.compare_sensor_map(past_state[1], present_state[1]),
            2: self.compare_rotators(past_state[2], present_state[2]),
            3: self.compare_component(past_state[3], present_state[3]),
            4: self.compare_rotators(past_state[4], present_state[4]),
            5: self.compare_component(past_state[5], present_state[5]),
            6: self.compare_component(past_state[6], present_state[6]),
            7: self.compare_component(past_state[7], present_state[7])
        }
        for i in range(len(past_state)):
            if not compare_dict.get(i):
                return False
        return True

    @staticmethod
    def compare_sensor_map(past_map, present_map):
        for x_coordinate in range(len(past_map)):
            for y_coordinate in range(len(past_map[x_coordinate])):
                if past_map[x_coordinate][y_coordinate] and present_map[x_coordinate][y_coordinate]:
                    if not past_map[x_coordinate][y_coordinate].sensor_area \
                           == present_map[x_coordinate][y_coordinate].sensor_area \
                            or not past_map[x_coordinate][y_coordinate].status \
                                   == present_map[x_coordinate][y_coordinate].status:
                        return False
        return True

    @staticmethod
    def compare_rotators(past_map, present_map):
        for x_coordinate in range(len(past_map)):
            for y_coordinate in range(len(past_map[x_coordinate])):
                if past_map[x_coordinate][y_coordinate] and present_map[x_coordinate][y_coordinate]:
                    if not past_map[x_coordinate][y_coordinate].direction \
                           == present_map[x_coordinate][y_coordinate].direction \
                            or not past_map[x_coordinate][y_coordinate].status \
                                   == present_map[x_coordinate][y_coordinate].status:
                        return False
        return True

    @staticmethod
    def compare_component(past_map, present_map):
        for x_coordinate in range(len(past_map)):
            for y_coordinate in range(len(past_map[x_coordinate])):
                if past_map[x_coordinate][y_coordinate] and present_map[x_coordinate][y_coordinate]:
                    if not past_map[x_coordinate][y_coordinate].status \
                           == present_map[x_coordinate][y_coordinate].status:
                        return False
        return True
