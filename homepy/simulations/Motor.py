import copy
import simpy

from homepy.simulations.Actuator import Actuator
from homepy.simulations.Camera import Camera
from homepy.simulations.MotionSensor import MotionSensor
"""
Models a motor component, capable of moving a device to another location
"""


class Motor(Actuator):
    def __init__(self, units, observer, env):
        """
        Constructor for the modeled motor.
        Parameters
        ----------
        observer
            The respective observer
        env
            The SimPy environment the simulation runs in
        """
        super(Motor, self).__init__(units, units[0].location, "MOTOR", observer, env)
        self.previous_location = self.location
        self.observer.motors[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The main process of the motor, continuously updating the its location based on the controllers' instruction
        Parameters
        ----------
        controller
            The control mechanism, giving directions to the motor
        habitat
            The habitat the simulated devices "live" in

        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                while not self.switch_to_manual_event.triggered:
                    next_location = self.controller.sensor_map[0][self.location[0]][self.location[1]]
                    self.move(next_location)
                    yield self.env.timeout(1)
                yield self.switch_to_auto_event
            except simpy.Interrupt:
                yield self.fixed_event

    def move(self, location):
        """
        Moves the motor and all components to the given location
        Parameters
        ----------
        location
            The location to be moved to
        """
        self.observer.motors[self.location[0]][self.location[1]] = 0
        self.observer.motors[location[0]][location[1]] = self
        for unit in self.units:
            self.move_unit(unit, self.location, location)
        self.location = location

    def move_unit(self, unit, previous, current):
        """
        Moves a component from a previous location to the current location
        Parameters
        ----------
        unit
            The unit that should be moved
        previous
            The units' previous location
        current
            The units' current location
        """
        if isinstance(unit, Camera):
            self.observer.cameras[previous[0]][previous[1]] = 0
            self.observer.cameras[current[0]][current[1]] = unit
        elif isinstance(unit, MotionSensor):
            self.observer.motionSensors[previous[0]][previous[1]] = 0
            self.observer.motionSensors[current[0]][current[1]] = unit
        unit.location = current

    def get_dummy(self):
        """
        Gets a dummy version of the motor, used for record keeping purposes
        Returns
        -------
        dummy_motor:DummyMotor
        """
        return DummyMotor(self)


class DummyMotor:
    """
    Dummy motor class, used for record keeping purposes
    """
    def __init__(self, motor):
        """
        The dummy motors' constructor
        Parameters
        ----------
        motor
            The motor the dummy version is based on
        """
        self.status = copy.copy(motor.status)
        self.type = copy.copy(motor.type)
        self.operation_mode = copy.copy(motor.operation_mode)
        self.priority = motor.habitat
        self.status = motor.status[:]
