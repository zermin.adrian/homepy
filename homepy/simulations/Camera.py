import numpy as np
import simpy

from homepy.simulations.Sensor import SensorArea, Sensor


"""
Models a camera that can perceive its environment.
"""


class Camera(Sensor):
    def __init__(self, location, direction, shape, dimension, client, observer, env):
        """
        Constructor of the camera.
        Parameters
        ----------
        location
            Location of the camera
        direction
            Direction the camera is pointing
        shape
            Shape of the cameras' sensed area
        dimension
            The cameras' range
        observer
            The current observer of the simulations
        env
            The SimPy environment the simulations runs in
        """
        super(Camera, self).__init__(direction, shape, dimension, location, "CAMERA", client, observer, env)
        self.current = []
        self.observer.cameras[self.location[0]][location[1]] = self

    def run(self, controller, habitat):
        """
        The run function of the camera. It keeps refreshing its sensor area and "seeing" everything in it.
        Parameters
        ----------
        habitat
            The habitat the simulated devices "live" in.
        controller
            The controller component that can influence any component during its lifetime.
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.sensor_area = SensorArea(self.direction, self.location, self.shape, self.dimension, self.observer.habitat)
                # Initialize the sensor area
                self.current = self.sense()
                bin_array = self.convert_to_binary(self.current)
                yield self.client.to_be_sent.put(len(bin_array))
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event  # Wait for the camera to be fixed
                self.current = []
                self.sensor_area = SensorArea(self.direction, self.location, self.shape, 0, self.observer.habitat)

    def sense(self):
        """
        The sense function of the camera. It detects what is in its sensor area.
        Returns
        -------
        sense_array
            An array of what the camera "saw"
        """
        sense_array = [[0 for x in range(self.habitat.map_size)] for y in range(self.habitat.map_size)]
        for i in self.sensor_area.coordinates:
            x = i[0]
            y = i[1]
            if self.habitat.map[x][y] != 1 and self.habitat.map[x][y] != 0:
                sense_array[x][y] = self.habitat.map[x][y].type
            else:
                sense_array[x][y] = self.habitat.map[x][y]
        return sense_array

    @staticmethod
    def convert_to_binary(sensor_array):
        """
        Converts the provided sensor array into a binary array.
        Parameters
        ----------
        sensor_array
            The sensor array.
        """
        bin_array = np.array(np.zeros((len(sensor_array), len(sensor_array))))
        for x in range(len(sensor_array)):
            for y in range(len(sensor_array[x])):
                bin_array[x][y] = bin(int(sensor_array[x][y]))[2:].zfill(8)
        return bin_array
