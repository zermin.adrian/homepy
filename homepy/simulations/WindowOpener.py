import copy
import simpy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Models a window opener.
"""


class WindowOpener(DeviceComponentInterface):
    def __init__(self, window, observer, env, location):
        """
        The window openers' constructor
        Parameters
        ----------
        window
            The window the opener can open
        observer
            The respective observer
        env
            The SimPy environment the simulation runs in
        location
            The components' location
        """
        self.window = window
        observer.window_openers[location[0]][location[1]] = self
        super(WindowOpener, self).__init__(1, "WINDOWOPENER", location, observer, env)

    def run(self, controller, habitat):
        """
        The window openers main process
        Parameters
        ----------
        controller
            The control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                if self.controller.window_opener_map[self.location[0]][self.location[1]] == 1 \
                        and not self.window.open_event.triggered:
                    self.window.open_event.succeed()
                if self.controller.window_opener_map[self.location[0]][self.location[1]] == 0 \
                        and not self.window.close_event.triggered:
                    self.window.close_event.succeed()
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def get_dummy(self):
        """
        Gets a dummy version of the window opener, used for record keeping purposes
        Returns
        -------
        dummy_window_opener:DummyWindowOpener
            The dummy window opener
        """
        return DummyWindowOpener(self)


class DummyWindowOpener:
    """
    Dummy window opener class, used for record keeping purposes
    """
    def __init__(self, window_opener):
        """
        The dummy window openers' constructor
        Parameters
        ----------
        window_opener
            The window opener the dummy is based on
        """
        self.status = copy.copy(window_opener.status)
        self.sub_process = False
