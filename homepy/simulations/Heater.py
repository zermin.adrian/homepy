import copy
import simpy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface


class Heater(DeviceComponentInterface):
    """
    Models a heating device component
    """
    def __init__(self, max_power, location, observer, env):
        """
        The components constructor
        Parameters
        ----------
        max_power
            The maximum power in [W]
        location
            The components location
        observer
            The habitat observer
        env
            The SimPy environment the simulation runs in
        """
        self.room = observer.habitat.find_room(location)
        self.current_temperature = observer.habitat.outside_temperature
        self.max_power = max_power
        self.power = 0
        observer.heaters[location[0]][location[1]] = self
        super(Heater, self).__init__(1, "HEATER", location, observer, env)
        self.sub_process = None

    def run(self, controller, habitat):
        """
        The main simulation process of a heater
        Parameters
        ----------
        controller
            The control mechanism of the heater
        habitat
            The habitat the simulated devices "live" in.
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                if self.power > 0:
                    self.sub_process = self.env.process(self.room.heat_up(self.power, self.current_temperature))
                    yield self.sub_process
                    self.sub_process = None
                else:
                    yield self.env.timeout(1)
            except simpy.Interrupt:
                if self.sub_process:
                    self.sub_process.interrupt()
                yield self.fixed_event

    def get_dummy(self):
        """
        Returns a dummy version of the heater, for record keeping purposes
        """
        return DummyHeater(self)


class DummyHeater:
    """
    Dummy class of the heater used for record keeping purposes
    """
    def __init__(self, heater):
        """
        Constructor of the dummy heater
        Parameters
        ----------
        heater
            The heater the dummy is based on
        """
        self.status = copy.copy(heater.status)
        self.power = copy.copy(heater.power)
        self.current_temperature = copy.copy(heater.current_temperature)
        self.max_power = copy.copy(heater.max_power)
        self.sub_process = heater.sub_process is not None
