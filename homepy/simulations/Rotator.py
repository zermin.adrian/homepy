import copy
import simpy

from homepy.simulations.Actuator import Actuator
"""
Models a rotating component, used to rotate cameras and other sensors
"""


class Rotator(Actuator):
    def __init__(self, units, observer, env):
        """
        The simulated rotators' constructor.
        Parameters
        ---------
        observer
            The responsible observer
        env
            The SimPy environment the simulation runs in
        """
        super(Rotator, self).__init__(units, units[0].location, "ROTATOR", observer, env)
        self.direction = self.units[0].direction
        self.observer.rotators[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The main process of the rotator, it continuously updated the direction the units are facing
        Parameters
        ----------
        controller
            The control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                while not self.switch_to_manual_event.triggered:
                    next_direction = self.controller.sensor_map[1][self.location[0]][self.location[1]]
                    self.change_direction(next_direction)
                    yield self.env.timeout(1)
                yield self.switch_to_auto_event
            except simpy.Interrupt:
                yield self.fixed_event

    def change_direction(self, direction):
        """
        Changes the current direction to the given one, for all units
        Parameters
        ----------
        direction
            The direction to turn to
        """
        if direction:
            self.direction = direction
            for unit in self.units:
                unit.direction = direction

    def get_dummy(self):
        """
        Gets a dummy version of th rotator, used for record keeping purposes
        Returns
        -------
        dummy_rotator:DummyRotator
        A dummy version of the rotator, used for record keeping purposes
        """
        return DummyRotator(self)


class DummyRotator:
    """
    Dummy rotator class, used for record keeping purposes
    """
    def __init__(self, rotator):
        """
        The dummy rotators constructor
        Parameters
        ----------
        rotator
            The rotator the dummy is based on
        """
        self.status = copy.copy(rotator.status)
        self.direction = copy.copy(rotator.direction)
        self.type = copy.copy(rotator.type)
        self.operation_mode = copy.copy(rotator.operation_mode)
        self.priority = rotator.habitat
