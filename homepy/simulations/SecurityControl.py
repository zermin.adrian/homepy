import copy

import simpy

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Models a security control mechanism device
"""


class SecurityControl(DeviceComponentInterface):
    def __init__(self, movement_patterns, observer, location, env):
        """
        The security control mechanism
        Parameters
        ----------
        movement_patterns
            The movement patterns for the controlled devices
        observer
            The respective observer
        location
            The components' location
        env
            The SimPy environment the simulation runs in
        """
        self.movement_patterns = movement_patterns
        super(SecurityControl, self).__init__(1, "SECURITYCONTROL",
                                              location, observer, env)
        observer.controls[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The main process of the security controller
        Parameters
        ----------
        controller
            The components' controller
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.learning = controller
        while True:
            try:
                for pattern in self.movement_patterns:
                    if pattern.operation == "MANUAL":
                        pattern.perform_action()
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def switch_to_auto(self, dislocator):
        """
        Switches the control unites' operating mode to automatic
        Parameters
        ----------
        dislocator
            The affected dislocator
        """
        dislocator.operation_mode = "AUTO"
        dislocator.switch_to_auto_event.succeed(value="MANUAL")
        dislocator.switch_to_auto_event = simpy.events.Event(self.env)
        for pattern in self.movement_patterns:
            if pattern.dislocator == dislocator:
                pattern.operation = "AUTO"

    def switch_to_manual(self, dislocator):
        """
        Switches the control unites' operating mode to manual
        Parameters
        ----------
        dislocator
            The affected dislocator
        """
        dislocator.operation_mode = "MANUAL"
        dislocator.switch_to_manual_event.succeed(value="MANUAL")
        dislocator.switch_to_manual_event = simpy.events.Event(self.env)
        for pattern in self.movement_patterns:
            if pattern.dislocator == dislocator:
                pattern.operation = "MANUAL"

    def get_dummy(self):
        """
        Gets a dummy version of the security control, used for record keeping purposes
        Returns
        -------
        dummy_control:DummyControl
            A dummy version of the security control
        """
        return DummyControl(self)


class MovementPattern:
    """
    Movement pattern class, used to describe movement patterns of devices
    """
    def __init__(self, start, end, dislocator, movement_mode, env):
        """
        Movement pattern constructor
        Parameters
        ----------
        start
            Start location or direction of the movement
        end
            End location or direction of the movement
        dislocatorpoint
            The component executing the movement
        movement_mode
            The operation mode of the movement
        env
            The SimPy environment the simulation runs in
        """
        self.env = env
        self.dislocator = dislocator
        self.start = start
        self.movement_direction = "COUNTERCLOCK"
        self.end = end
        self.movement_mode = movement_mode
        self.operation = "MANUAL"
        self.dislocator.switch_to_manual_event.succeed()
        self.dislocator.switch_to_manual_event = simpy.events.Event(self.env)
        self.current_dict = {
            "ROTATE": self.dislocator.direction,
            "PATROL": self.dislocator.location,
            "STATIONARY": self.dislocator.location
        }
        self.current = self.current_dict.get(self.movement_mode)

    def perform_action(self):
        """
        Perform action based on the movement pattern.
        """
        if self.current == self.end or self.current == self.start:
            self.movement_direction = self.get_next_mode()
        if self.movement_mode == "ROTATE":
            next = self.get_next_direction()
            self.dislocator.change_direction(next)
            self.current = next
        if self.movement_mode == "PATROL":
            next = self.get_next_location()
            self.dislocator.move(next)
            self.current = next

    def get_next_mode(self):
        """
        Returns next movement mode/direction
        """
        if self.movement_direction == "COUNTERCLOCK":
            return "CLOCK"
        else:
            return "COUNTERCLOCK"

    def get_next_direction(self):
        """
        Gives the next direction according to the movement pattern
        Returns
        -------
        direction:str
            The next direction.
        """
        directions = ["N", "NW", "W", "SW", "S", "SE", "E", "NE"]
        index = directions.index(self.current)
        if self.movement_direction == "COUNTERCLOCK":
            if self.current == "NE":
                return "N"
            else:
                return directions[index + 1]
        if self.movement_direction == "CLOCK":
            if self.current == "N":
                return "NE"
            else:
                return directions[index - 1]

    def get_next_location(self):
        """
        Gives the next location based on the movement pattern
        Returns
        -------
        location:list
            Location of the format [x, y]
        """
        diffx = self.start[0] - self.end[0]
        diffy = self.start[1] - self.end[1]
        movement_vector = [0, 0]
        location = self.current[:]
        if diffx != 0:
            movement_vector[0] = 1
        if diffy != 0:
            movement_vector[1] = 1
        if self.movement_direction == "CLOCK":
            location[0] -= movement_vector[0]
            location[1] -= movement_vector[1]
        if self.movement_direction == "COUNTERCLOCK":
            location[0] += movement_vector[0]
            location[1] += movement_vector[1]
        return location

    def get_dummy(self):
        """
        Gets the dummy version of the movement pattern, used for record keeping purposes
        Returns
        -------
        dummy:DummyMovementPattern
            The dummy version of the movement pattern
        """
        return DummyMovementPattern(self)


class DummyControl:
    """
    The dummy class of the control unit, used for record keeping purposes
    """
    def __init__(self, control):
        """
        The dummy control units' constructor
        Parameters
        ----------
        control:SecurityControl
            The control unit the dummy is based on
        """
        self.location = control.location[:]
        self.type = copy.copy(control.type)
        self.status = copy.copy(control.status)
        self.movement_patterns = [movement_pattern.get_dummy() for movement_pattern in control.movement_patterns]
        self.priority = control.habitat


class DummyMovementPattern:
    """
    A dummy movement pattern class used for record keeping purposes
    """
    def __init__(self, movement_pattern):
        """
        The dummy versions' constructor
        Parameters
        ----------
        movement_pattern:MovementPattern
            The movement pattern the dummy is based on
        """
        self.start = copy.copy(movement_pattern.start)
        self.movement_direction = copy.copy(movement_pattern.movement_direction)
        self.end = copy.copy(movement_pattern.end)
        self.movement_mode = copy.copy(movement_pattern.movement_mode)
        self.operation = copy.copy(movement_pattern.operation)
        self.current = copy.copy(movement_pattern.current)
