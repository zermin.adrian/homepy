import copy
import simpy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Models a dimmable light bulb component
"""


class LightBulb(DeviceComponentInterface):
    def __init__(self, lumen, max_power, location, observer, env):
        """
        The simulated light bulbs' constructor
        Parameters
        ----------
        lumen
            The lumen provided by the light bulb
        max_power
            The maximum possible power of the light bulb
        location
            The light bulb location
        observer
            The habitat observer
        env
            The SimPy environment the simulation runs in
        """
        self.room = observer.habitat.find_room(location)
        observer.light_bulbs[location[0]][location[1]] = self
        self.lumen = lumen
        self.power = 0
        self.max_power = max_power
        super(LightBulb, self).__init__(1, "LIGHTBULB", location, observer, env)

    def run(self, controller, habitat):
        """
        The main run process of the light bulb
        Parameters
        ----------
        controller
            The control mechanism of the light bulb
        habitat
            The habitat the simulated devices "live" in
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                self.power = self.max_power * self.controller.dimmer_map[self.location[0]][self.location[1]]
                if self.power > 0:
                    self.room.illumination += \
                        (self.lumen * (self.power/self.max_power)) / (len(self.room.coordinates)/25)    # [lx or lm/m²]
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def get_dummy(self):
        """
        Gets the dummy version of the light bulb used for record keeping purposes
        Returns
        -------
        dummy_light_bulb:DummyLightBulb
            The dummy light bulb used for record keeping purposes
        """
        return DummyLightBulb(self)


class DummyLightBulb:
    """
    The dummy class of the light bulb, used for record keeping purposes
    """
    def __init__(self, light_bulb):
        """
        The dummy class' constructor
        Parameters
        ----------
        light_bulb
            The light bulb the dummy is based on
        """
        self.lumen = copy.copy(light_bulb.lumen)
        self.status = copy.copy(light_bulb.status)
        self.power = copy.copy(light_bulb.power)
        self.max_power = copy.copy(light_bulb.max_power)
        self.sub_process = False
