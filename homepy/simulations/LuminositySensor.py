from homepy.simulations.Sensor import Sensor
import numpy as np
import simpy
"""
Models a light sensor 
"""


class LuminositySensor(Sensor):
    def __init__(self, location, client, observer, env):
        """
        Constructor for light sensor
        Parameters
        ----------
        location
            The components' location
        client
            The client sending out the sensory data
        observer
            The habitat observer the sensor reports to
        env
            The SimPy environment the simulation runs in
        """
        super(LuminositySensor, self).__init__("N", "SELF", 1, location, "LUMINOSITYSENSOR", client, observer, env)
        self.current = None
        self.observer.luminosity_sensors[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The main process of the sensor.
        Parameters
        ----------
        controller
            The sensors' control mechanism
        habitat
            The habitat the simulated devices "live" in
        Returns
        -------

        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.current = self.sense()
                if self.current:
                    self.observer.history.room_luminosities.append(self.current)
                else:
                    self.observer.history.room_luminosities.append(0)
                print("LIGHT: " + str(self.current))
                bin_array = self.convert_to_binary(self.current)
                yield self.client.to_be_sent.put(len(bin_array))
                yield self.env.timeout(1)
            except simpy.Interrupt:
                self.current = None
                yield self.fixed_event

    def sense(self):
        """
        Senses the current amount of light at the location
        Returns
        -------
        lux
            The amount of light in the room
        """
        return self.habitat.get_luminosity(self.location)

    @staticmethod
    def convert_to_binary(luminosity):
        """
        Converts the given value into a binary string
        Parameters
        ----------
        luminosity
            The sensed luminosity

        Returns
        -------
        bin_array:list
            The converted binary array
        """
        bin_array = np.array(1)
        bin_array = bin(int(luminosity))[2:].zfill(8)
        return bin_array