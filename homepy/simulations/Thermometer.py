import simpy

from homepy.simulations.Sensor import Sensor
"""
Models a thermometer component
"""


class Thermometer(Sensor):
    def __init__(self, location, client, observer, env):
        """
        The thermometers' constructor
        Parameters
        ----------
        location
            The components' location
        client
            The client the thermometer reports to
        observer
            The respective observer the thermometer reports to
        env
            The SimPy environment the simulation runs in
        """
        observer.thermometers[location[0]][location[1]] = self
        super(Thermometer, self).__init__("N", "SELF", 1, location, "THERMOMETER", client, observer, env)

    def run(self, controller, habitat):
        """
        The thermometers main process, continuously updates the sensed temperature
        Parameters
        ----------
        controller
            The control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.current = self.sense()
                if self.current:
                    self.observer.history.room_temperatures.append(self.current)
                else:
                    self.observer.history.room_temperatures.append(0)
                print("Temperature: "+str(self.current))
                bin_array = self.convert_to_binary(self.current)
                yield self.client.to_be_sent.put(len(bin_array))
                yield self.env.timeout(1)
            except simpy.Interrupt:
                self.current = None
                yield self.fixed_event

    def sense(self):
        """
        Senses the current temperature
        Returns
        -------
        temperature:int
            The sensed temperature
        """
        return self.observer.habitat.get_temperature(self.location)

    @staticmethod
    def convert_to_binary(temperature):
        """
        Converts the given temperature into a binary array.
        Parameters
        ----------
        temperature
            The temperature to be converted

        Returns
        -------
        bin_array:list
            The converted binary array
        """
        bin_array = bin(int(temperature))[2:].zfill(8)
        return bin_array