from homepy.simulations.Sensor import Sensor
import numpy as np
import simpy

"""
Models a  device component to measure relative humidity
"""


class RelativeHumiditySensor(Sensor):
    def __init__(self, location, client, observer, env):
        """
        The sensors' constructor
        Parameters
        ----------
        location
            The components' location
        client
            The client the sensor reports to
        observer
            The responsible observer
        env
            The SimPy environment the simulation runs in
        """
        super(RelativeHumiditySensor, self).__init__("N", "SELF", 1, location, "REALTIVEHUMIDITYSENSOR", client, observer, env)
        observer.humidity_sensors[location[0]][location[1]] = self

    def run(self, controller, habitat):
        """
        The main process of the sensor, used to continuously update the sensed values and give them to the client.
        Parameters
        ----------
        controller
            The sensors' control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.current = self.sense()
                if self.current:
                    self.observer.history.room_humidities.append(self.current)
                else:
                    self.observer.history.room_humidities.append(0)
                print("Humidity:" + str(self.current))
                bin_array = self.convert_to_binary(self.current)
                yield self.client.to_be_sent.put(len(bin_array))
                yield self.env.timeout(1)
            except simpy.Interrupt:
                self.current = None
                yield self.fixed_event

    def sense(self):
        """
        Senses the current relative humidity.
        Returns
        -------
        relative_humidity:int
            The relative humidity.
        """
        return self.habitat.get_relative_humidity(self.location)

    @staticmethod
    def convert_to_binary(humidity):
        """
        Converts the given humidity to a binary array
        Parameters
        ----------
        humidity
            The current humidity

        Returns
        -------
        bin_array
            A binary representation of the humidity
        """
        bin_array = np.array(1)
        bin_array = bin(int(humidity))[2:].zfill(8)
        return bin_array
