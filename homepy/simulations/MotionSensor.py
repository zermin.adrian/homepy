import numpy as np
import simpy

from homepy.simulations.Sensor import Sensor, SensorArea
"""
Models a motion sensor device component
"""


class MotionSensor(Sensor):
    def __init__(self, location, direction, shape, dimension, client, observer, env):
        """
        The simulated motion sensors' constructor
        Parameters
        ----------
        location
            The components' location
        direction
            The direction the motion sensor points in
        shape
            The shape of the sensed area
        dimension
            The reach or dimension of the sensor
        client
            The client that sends out the sensory data
        observer
            The respective observer
        env
            The SimPy environment the simulation runs in
        """
        super(MotionSensor, self).__init__(direction, shape, dimension, location,
                                           "MOTIONSENSOR", client, observer, env)
        self.previous = None
        self.observer.motionSensors[self.location[0]][location[1]] = self
        self.change_map = []
        self.current = None

    def run(self, controller, habitat):
        """
        The main process of the motion sensor
        Parameters
        ----------
        controller
            The components' control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.sensor_area = SensorArea(self.direction, self.location, self.shape, self.dimension,
                                              self.observer.habitat)
                self.current = self.sense()
                if self.previous is not None:
                    self.change_map = self.compare_areas(self.previous, self.current)
                    bin_array = self.convert_to_binary(self.change_map)
                    yield self.client.to_be_sent.put(len(bin_array))
                yield self.env.timeout(1)
                self.previous = self.current
            except simpy.Interrupt:
                self.current = None
                self.previous = None
                self.sensor_area = SensorArea(self.direction, self.location, self.shape, 0, self.observer.habitat)
                yield self.fixed_event

    def sense(self):
        """
        Senses the change in the sensor area
        Returns
        -------
        sense_array:list
            The sense array of the motion sensor
        """
        sense_array = np.zeros((100, 100))
        for i in self.sensor_area.coordinates:
            x = i[0]
            y = i[1]
            if self.habitat.map[x][y]:
                sense_array.put([x, y], 1)
            else:
                sense_array.put([x, y], 0)
        return sense_array

    def compare_areas(self, area_1, area_2):
        """
        Compares two sensor areas for changes
        Parameters
        ----------
        area_1
            One of the sensor areas to be compared
        area_2
            One of the sensor areas to be compared
        Returns
        -------
        change_map
            A map of changes between the two sensed arrays
        """
        change_map = np.zeros((len(area_1), len(area_1)))
        for x_coordinate in range(len(area_1)):
            for y_coordinate in range(len(area_1)):
                if area_1[x_coordinate][y_coordinate] != area_2[x_coordinate][y_coordinate]:
                    change_map[x_coordinate][y_coordinate] = 1
        return change_map

    @staticmethod
    def convert_to_binary(sensor_array):
        """
        Converts the given sensor array to binary
        Parameters
        ----------
        sensor_array
            The sensed array of the motion sensor
        Returns
        -------
        bin_array
            Binary array to be sent by the client
        """
        bin_array = np.array(np.zeros((len(sensor_array), len(sensor_array))))
        for x_coordinate in range(len(sensor_array)):
            for y_coordinate in range(len(sensor_array[x_coordinate])):
                bin_array[x_coordinate][y_coordinate] = bin(int(sensor_array[x_coordinate][y_coordinate]))[2:].zfill(8)
        return bin_array
