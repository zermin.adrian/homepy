from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
import copy
import simpy

"""
Models an air cooler that can cool down its environment.
"""


class AirCooler(DeviceComponentInterface):
    def __init__(self, max_btu_per_hour, max_power, location, observer, env):
        """
        Constructor for the simulated air cooler.
        Parameters
        ----------
        max_btu_per_hour
            Maximum amount of energy that can be absorbed per hour, in the british thermal unit [BTU].
            1 [BTU] = 1055 [J]
        max_power
            The maximum power consumed by the air cooler.
        location
            The location of the air cooler.
        observer
            The habitat observer.
        env
            The SimPy environment the simulation runs in.
        """
        self.room = observer.habitat.find_room(location)
        self.max_btu_per_hour = max_btu_per_hour
        self.max_power = max_power
        self.power = 0
        observer.coolers[location[0]][location[1]] = self
        super(AirCooler, self).__init__(1, "AIRCOOLER", location, observer, env)
        self.sub_process = None

    def run(self, controller, habitat):
        """
        The run process of the air cooler. It cools down the room depending on the power provided to it.
        Parameters
        ----------
        controller
            The control mechanism of the air cooler.
        habitat
            The habitat the simulated devices "live" in.
        """
        self.learning = controller
        self.habitat = habitat
        while True:
            try:
                if self.power > 0:
                    self.sub_process = self.env.process(self.room.cool_down(self.power, self.max_power,
                                                                            self.max_btu_per_hour))  # start cooling process
                    yield self.sub_process
                    self.sub_process = None
                else:
                    yield self.env.timeout(1)
            except simpy.Interrupt:
                if self.sub_process:
                    self.sub_process.interrupt()
                yield self.fixed_event

    def get_dummy(self):
        """
        Returns a dummy version of the air cooler for recoding purposes.
        Returns
        -------
        dummy_cooler:DummyCooler
            A dummy version of the air cooler.

        """
        return DummyCooler(self)


class DummyCooler:
    """
    Dummy class of the air cooler. Used for historic copies.
    """

    def __init__(self, cooler):
        """
        Constructor of the dummy air cooler.
        Parameters
        ----------
        cooler
            The original air cooler.
        """
        self.power = copy.copy(cooler.power)
        self.max_power = copy.copy(cooler.max_power)
        self.max_btu_per_hour = copy.copy(cooler.max_btu_per_hour)
        self.status = copy.copy(cooler.status)
        self.priority = copy.copy(cooler.habitat)
        self.sub_process = cooler.sub_process is not None
