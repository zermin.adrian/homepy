import simpy
import copy

"""
Models a data package, to be sent to a certain destination.
"""


class DataPackage:
    def __init__(self, env, source, current, size, destination, situation):
        """
        The constructor for a data package
        Parameters
        ----------
        env
            The SimPy environment the simulations runs in
        source
            The source of the data package, where it was generated
        current
            The current device where the data package is
        destination
            The packages' destination
        situation
            The current observer of the simulations
        """
        self.size = size
        self.source = source
        self.type = "PACKAGE"
        self.destination = destination
        self.current = current
        self.env = env
        self.creation_time = env.now
        self.arrival_time = None
        situation.packages.append(self)
        self.next = None
        self.path = []
        self.hop_count = 0
        self.arrived_event = simpy.events.Event(self.env)
        self.load = []
        # Event that gets triggered once a package has arrived at a router
        self.routing_event = simpy.events.Event(self.env)
        # Event that gets triggered when a  package is getting routed
        self.sent_event = simpy.events.Event(self.env)
        # Event that gets triggered when a package has been sent
        self.lost_event = simpy.events.Event(self.env)
        # Event that gets triggered when a package has been lost
        self.destination_event = simpy.events.Event(self.env)
        # Event that gets triggered when a package has arrived at its destination

    def run(self):
        """
        This function has the package wait for any of its events to get triggered and handles them.
        """
        while True:
            events = [self.arrived_event, self.routing_event, self.sent_event, self.destination_event, self.lost_event]
            a = yield simpy.AnyOf(self.env, events)
            if self.arrived_event in a:
                self.arrived_event = simpy.events.Event(self.env)
            elif self.routing_event in a:
                self.routing_event = simpy.events.Event(self.env)
            elif self.sent_event in a:
                self.sent_event = simpy.events.Event(self.env)
            elif self.lost_event in a:
                break
            else:
                self.destination_event = simpy.events.Event(self.env)
            yield self.env.timeout(1)

    def get_dummy(self):
        return DummyDataPackage(self)


class DummyDataPackage:
    def __init__(self, data_package):
        self.destination = copy.copy(data_package.destination.connection_id)
        self.size = copy.copy(data_package.size)
        self.creation_time = copy.copy(data_package.creation_time)
        self.arrival_time = copy.copy(data_package.arrival_time)
