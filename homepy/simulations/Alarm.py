import copy

import simpy

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface

"""
Models an Alarm device that can be triggered or turned off by other devices.
"""


class Alarm(DeviceComponentInterface):
    def __init__(self, volume, location, observer, env):
        """
        Constructor of the Alarm
        Parameters
        ----------
        volume : int
            The volume of the Alarm
        location : list
            The alarms' location
        observer
            The current observer of the simulations
        env
            SimPy environment the simulations runs in
        """
        super(Alarm, self).__init__(1, "ALARM", location, observer, env)
        self.volume = volume
        self.triggered = 0
        self.siren_event = simpy.events.Event(self.env)     # Event that triggers the alarm process
        self.turn_off_event = simpy.events.Event(self.env)      # Event that can be triggered to turn off the alarm
        self.siren_event.callbacks.append(self.alarm_starter)
        self.turn_off_event.callbacks.append(self.turn_off)
        observer.alarms[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The run process of the alarm. It waits to be triggered and if so, triggers a siren event.
        Parameters
        ----------
        controller
            The control mechanism for the alarm.
        habitat
            The habitat the simulated devices "live" in.
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                yield self.siren_event
            except simpy.Interrupt:
                yield self.fixed_event
                self.triggered = 0
                self.siren_event = simpy.events.Event(self.env)
                self.turn_off_event = simpy.events.Event(self.env)
                self.siren_event.callbacks.append(self.alarm_starter)
                self.turn_off_event.callbacks.append(self.turn_off)

    def alarm_starter(self, event):
        """
        Callback function to trigger the alarm process.
        Parameters
        ----------
        event
            The event that triggered the callback.
        """
        self.env.process(self.alarm())

    def alarm(self):
        """
        The alarm process, it keeps going until turned off.
        """
        self.triggered = 1
        print("Alarm was triggered, calling security...")
        yield self.env.timeout(2)
        print("Security arriving...")
        while not self.turn_off_event.triggered:
            print("INTRUDER DETECTED!")
            yield self.env.timeout(1)
        self.triggered = 0
        self.siren_event = simpy.events.Event(self.env)
        self.siren_event.callbacks.append(self.alarm_starter)

    def turn_off(self, event):
        """
        Callback function for turning the alarm off.
        Parameters
        ----------
        event
            Event that triggered the callback.
        """
        print("Turning off siren")
        self.turn_off_event = simpy.events.Event(self.env)
        self.turn_off_event.callbacks.append(self.turn_off)

    def get_dummy(self):
        """
        Returns a dummy version of the provided alarm.
        Returns
        -------
        dummy_alarm: DummyAlarm
            A dummy version of the provided alarm object.
        """
        return DummyAlarm(self)


class DummyAlarm:
    """
    Dummy class for recording purposes.
    """
    def __init__(self, alarm):
        """
        Constructor of the dummy alarm.
        Parameters
        ----------
        alarm
            The alarm object the dummy alarm is based on.
        """
        self.status = copy.copy(alarm.status)
        self.triggered = copy.copy(alarm.triggered)
