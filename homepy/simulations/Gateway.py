import simpy

from homepy.simulations.Connectable import Connectable

"""
Models a gateway for simulating a wireless mesh network.
"""

GHZ_5 = [-30, -30, -30, -30, -30, -47, -47, -47, -47, -47, -53, -53, -53, -53, -53, -57, -57, -57, -57, -57,
         -59, -59, -59, -59, -59, -61, -61, -61, -61, -61, -63, -63, -63, -63, -63, -64, -64, -64, -64, -64,
         -65, -65, -65, -65, -65, -66, -66, -66, -66, -66, -67, -67, -67, -67, -67]
GHZ_2_4 = [-30, -30, -30, -30, -30, -40, -40, -40, -40, -40, -46, -46, -46, -46, -46, -49, -49, -49, -49, -49,
           -52, -52, -52, -52, -52, -54, -54, -54, -54, -54, -56, -56, -56, -56, -56, -57, -57, -57, -57, -57,
           -58, -58, -58, -58, -58, -59, -59, -59, -59, -59, -60,  -60, -60, -60, -60, -61, -61, -61, -61, -61,
           -61.5, -61.5, -61.5, -61.5, -61.5, -62, -62, -62, -62, -62, -63, -63, -63, -63, -63,
           -63.5, -63.5, -63.5, -63.5, -63.5, -64, -64, -64, -64, -64, -64.5, -64.5, -64.5, -64.5, -64.5,
           -65, -65, -65, -65, -65, 65.5, 65.5, 65.5, 65.5, 65.5, -66, -66, -66, -66, -66,
           -66.5, -66.5, -66.5, -66.5, -66.5, -67, -67, -67, -67, -67]


class Gateway(Connectable):
    def __init__(self, route_delay, frequency, location, observer, env):
        """
        The gateways' constructor.
        Parameters
        ----------
        route_delay
            Delay required for routing
        location
            The components location
        observer
            The current observer of the simulations
        env
            The SimPy environment the simulations runs in
        """
        super(Gateway, self).__init__(7000000000, 0.98, location, "GATEWAY", observer, env)
        self.frequency = frequency
        self.route_delay = route_delay
        self.min_packet_size = 21
        self.max_packet_size = 65535
        self.transmission_bit_error_probability = 0.98
        observer.routers.append(self)

    def run(self, controller, habitat):
        """
        The run process of the simulated gateway. Continuously routes incoming packages and acts as
        a destination for packages.

        Parameters
        ----------
        habitat
            The habitat the simulated devices "live" in.
        controller
            The control mechanism the gateways actions are based on.
        """
        self.connection = simpy.Store(self.env, 1)
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                self.set_signal_strengths()
                self.current_package = yield self.connection.get()
                self.current_package.hop_count += 1
                self.current_package.path.append(self.connection_id)
                self.queue_length = len(self.connection.put_queue)
                del self.destination_queue[0]
                if self.current_package.destination != self:
                    while self.controller.routing_table[self.connection_id][
                        self.current_package.destination.connection_id] is None:
                        yield self.env.timeout(1)
                    self.current_package.routing_event.succeed(value="ROUTING")
                    yield self.env.timeout(self.route_delay)
                    sending_to = self.observer.connectables[
                        int(self.controller.routing_table[self.connection_id][
                                self.current_package.destination.connection_id])]
                    self.current_package.next = sending_to
                    self.current_package.sent_event.succeed(value="SENT")
                    self.sub_process = self.env.process(self.send_package(self.current_package))
                    yield self.sub_process
                else:
                    self.current_package.arrival_time = self.env.now
                    total_send_time = self.current_package.arrival_time - self.current_package.creation_time
                    self.observer.throughput_times.append(total_send_time)
                    self.observer.arrived_packages.append(self.current_package)
                    print("Package Arrived")
                    self.current_package.arrived_event.succeed(value="ARRIVED")
                    self.current_package = None
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event
                if self.sub_process:
                    if self.sub_process.is_alive:
                        self.sub_process.interrupt()
                for j in self.observer.packages:
                    if j.current == self:
                        j.lost_event.succeed(value="LOST")
                        self.observer.packages.remove(j)
                self.destination_queue = []
                self.queue_length = 0
                self.connection = simpy.Store(self.env, 1)
                self.current_package = None
                for coordinate in self.get_coordinates():
                    self.observer.signal_strengths[coordinate[0]][coordinate[1]][self.connection_id] = 0

    def send_package(self, data_package):
        """
        Sends a package to its next hop, according to the routing table.
        Parameters
        ----------
        data_package
            The data package to be sent.
        """
        try:
            send_delay = self.compute_send_delay(data_package.size,
                                                 self.observer.signal_strengths[self.location[0]][self.location[1]][
                                                     data_package.next.connection_id])
            yield self.env.timeout(send_delay)
            if data_package.next.status:
                data_package.next.destination_queue.append(data_package.destination.connection_id)
                data_package.next.connection.put(data_package)
                data_package.current = data_package.next
            else:
                data_package.next = self
                data_package.next.destination_queue.append(data_package.destination.connection_id)
                data_package.next.connection.put(data_package)
        except simpy.Interrupt:
            print("sending process interrupted")

    def set_signal_strengths(self):
        """
        Sets the signal strengths based on its own location, distance and the walls between two locations.
        """
        coordinates = self.get_coordinates()
        for coordinate in coordinates:
            walls = self.get_walls(self.location, coordinate)
            distance = max([abs(self.location[0] - coordinate[0]), abs(self.location[1] - coordinate[1])])
            signal_strength = self.get_signal_strength(walls, distance)
            self.observer.signal_strengths[coordinate[0]][coordinate[1]][self.connection_id] = signal_strength

    def get_coordinates(self):
        """
        Returns the coordinates of all locations within the range of the Gateway
        Returns
        -------
        coordinates:list
            A list of coordinates within the routers theoretical reach.
        """
        coordinates = []
        if self.frequency == 2.4:
            for x_dist in range(-111, 111):
                for y_dist in range(-111, 111):
                    coordinate = [self.location[0] + x_dist, self.location[1] + y_dist]
                    if coordinate not in coordinates and coordinate[0] < 100 > coordinate[1]:
                        coordinates.append(coordinate)
            return coordinates
        else:
            for x_dist in range(-51, 51):
                for y_dist in range(-51, 51):
                    coordinate = [self.location[0] + x_dist, self.location[1] + y_dist]
                    if coordinate not in coordinates and 0 <= coordinate[0] < 100 > coordinate[1] >= 0:
                        coordinates.append(coordinate)
            return coordinates

    def get_walls(self, coordinate_1, coordinate_2):
        """
        Returns the number of walls between two locations.
        Parameters
        ----------
        coordinate_1:list
            Location with a format [x, y]
        coordinate_2:list
            Location with a format [x, y]
        Returns
        -------
        number_of_walls:int
            The number of walls between the two locations.
        """
        wall_coordinates = []
        walls = 0
        interference_box = []
        diffx = coordinate_1[0] - coordinate_2[0]
        diffy = coordinate_1[1] - coordinate_2[1]
        y_coordinates = []
        x_coordinates = []
        for distance in range(diffy):
            y_coordinates.append(coordinate_1[0] + distance)
        for distance in range(diffx):
            x_coordinates.append(coordinate_1[1] + diffx)
        for x_coordinate in x_coordinates:
            for y_coordinate in y_coordinates:
                if 0 <= x_coordinate < 100 and 0 <= y_coordinate < 100:
                    interference_box.append([x_coordinate, y_coordinate])
        walls = []
        for coordinate in interference_box:
            if self.habitat.map[coordinate[0]][coordinate[1]]:
                for wall in walls:
                    if [coordinate[0] - 1, coordinate[1]] or [coordinate[0], coordinate[1] - 1] in wall:
                        wall.append(coordinate)
                    else:
                        walls.append([coordinate])
        return len(walls)

    def get_signal_strength(self, number_of_walls, distance):
        """
        Calculates the signal strength based on the number of walls between two points and the distance
        Parameters
        ----------
        number_of_walls:int
            The number of walls between two points
        distance
            The distance between two points

        Returns
        -------
        strength:int
            Signal strength in [dbm]
        """
        strength = 0
        if self.frequency == 2.4:
            if distance > len(GHZ_2_4) - 1:
                strength = 0
            else:
                strength = GHZ_2_4[distance]
        else:
            if distance > len(GHZ_5) - 1:
                strength = 0
            else:
                strength = GHZ_5[distance]
        strength -= (3 * number_of_walls)
        if strength >= -67:
            return strength
        else:
            return 0
