import copy
import simpy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Models a thermostat device component, used to control heaters or coolers.
"""


class Thermostat(DeviceComponentInterface):
    def __init__(self, heaters, coolers, observer, env, location):
        """
        The thermostats' constructor
        Parameters
        ----------
        heaters
            The heaters the thermostat controls
        coolers
            The coolers the thermostat controls
        observer
            The respective observer
        env
            The SimPy environment the simulation runs in
        location
            The components' location
        """
        self.room = observer.habitat.find_room(location)
        self.coolers = coolers
        self.heaters = heaters
        observer.thermostats[location[0]][location[1]] = self
        super(Thermostat, self).__init__( 1, "THERMOSTAT", location, observer, env)

    def run(self, controller, habitat):
        """
        The thermostats' main process, controlling the heaters and coolers
        Parameters
        ----------
        controller
            The control mechanism
        habitat
            The habitat the simulated devices "live" in
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                for heater in self.heaters:
                    heater_action = self.controller.heater_map[heater.location[0]][heater.location[1]]
                    if heater_action:
                        heater.power = heater_action[0] * heater.max_power
                        heater.current_temperature = heater_action[1]
                for cooler in self.coolers:
                    cooler_action = self.controller.cooler_map[cooler.location[0]][cooler.location[1]]
                    if cooler_action:
                        cooler.power = cooler.max_power * cooler_action
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def get_dummy(self):
        """
        Gets the dummy version of the thermostat, used for record keeping purposes
        """
        return DummyThermostat(self)


class DummyThermostat:
    """
    Dummy class of the thermostat, used for record keeping purposes
    """
    def __init__(self, thermostat):
        """
        The dummy thermostats' constructor
        Parameters
        ----------
        thermostat
            The thermostat the dummy is based on
        """
        self.coolers = [cooler.get_dummy() for cooler in thermostat.coolers]
        self.heaters = [heater.get_dummy() for heater in thermostat.heaters]
        self.status = copy.copy(thermostat.status)
        self.sub_process = False
