import simpy

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface

"""
Abstract base class, for any simulated actuators, that can influence other components by moving them or rotating them.
"""


class Actuator(DeviceComponentInterface):

    def __init__(self, units, location, device_type, observer, env):
        """
        The constructor of an actuator
        Parameters
        ----------
        units
            The units the actuator can move or rotate
        location
            Location of the actuator
        device_type
            Type of the device
        observer
            The current observer of the simulations
        env
            The SimPy environment the simulations runs in
        """
        self.operation_mode = "AUTO"
        self.units = units
        self.switch_to_auto_event = simpy.events.Event(env)
        self.switch_to_manual_event = simpy.events.Event(env)
        super(Actuator, self).__init__(1, device_type, location, observer, env)

    def run(self, controller, habitat):
        """
        Main process for any actuator.
        Parameters
        ----------
        controller
            The control mechanism giving the actuator instructions.
        habitat
            The habitat the simulated devices "live" in.
        Returns
        -------

        """
        pass
