import copy
import simpy

from homepy.simulations.Actuator import Actuator
from homepy.simulations.Camera import Camera
from homepy.simulations.MotionSensor import MotionSensor

"""
Models a dislocator, a component that can both rotate and move a device
"""


class Dislocator(Actuator):
    def __init__(self, units, observer, env):
        """
        The actuators' constructor

        Parameters
        ----------
        observer
            The current observer of the simulations
        env
            The simpy environment the simulations runs in
        """
        super(Dislocator, self).__init__(units, units[0].location, "DISLOCATOR", observer, env)
        self.previous_location = self.location
        self.direction = units[0].direction
        self.observer.dislocators[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The dislocators' run process, it updates the location and direction of its units according to a sensor map.
        Parameters
        ----------
        habitat
            The habitat the simulated devices "live" in.
        controller
            The controller component that provides the sensor map

        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                while not self.switch_to_manual_event.triggered:
                    next_location = self.controller.sensor_map[0][self.location[0]][self.location[1]]
                    next_direction = self.controller.sensor_map[1][self.location[0]][self.location[1]]
                    if next_location == 0 or next_direction == 0:
                        yield self.env.timeout(1)
                        next_location = self.controller.sensor_map[0][self.location[0]][self.location[1]]
                        next_direction = self.controller.sensor_map[1][self.location[0]][self.location[1]]
                    self.move(next_location)
                    self.change_direction(next_direction)
                    yield self.env.timeout(1)
                yield self.switch_to_auto_event
            except simpy.Interrupt:
                yield self.fixed_event

    def move(self, location):
        """
        Function to move the dislocator and its connected units
        Parameters
        ----------
        location
            The new location
        """
        self.observer.dislocators[self.location[0]][self.location[1]] = 0
        self.observer.dislocators[location[0]][location[1]] = self
        for unit in self.units:
            self.move_unit(unit, self.location, location)
        self.location = location

    def move_unit(self, unit, previous_location, next_location):
        """
        Function to move a specific unit
        Parameters
        ----------
        unit
            Unit to be moved
        previous_location
            The previous location
        next_location
            The current location
        """
        if isinstance(unit, Camera):
            self.observer.cameras[previous_location[0]][previous_location[1]] = 0
            self.observer.cameras[next_location[0]][next_location[1]] = unit
        elif isinstance(unit, MotionSensor):
            self.observer.motionSensors[previous_location[0]][previous_location[1]] = 0
            self.observer.motionSensors[next_location[0]][next_location[1]] = unit
        unit.location = next_location

    def change_direction(self, direction):
        """
        Function to change the direction
        Parameters
        ----------
        direction
            New direction
        """
        self.direction = direction
        for unit in self.units:
            unit.direction = direction

    def get_dummy(self):
        """
        Function to get a dummy-like copy of the dislocator, used for keeping history.

        Returns
        -------
        dummy_dislocator:DummyDislocator
            A dummy dislocator object, based on a simulated dislocator.
        """
        return DummyDislocator(self)


class DummyDislocator:
    """
    Dummy class for dislocator, used to save to history.
    """
    def __init__(self, dislocator):
        self.status = copy.copy(dislocator.status)
        self.location = dislocator.location[:]
        self.direction = copy.copy(dislocator.direction)
        self.type = copy.copy(dislocator.type)
        self.operation_mode = copy.copy(dislocator.operation_mode)
