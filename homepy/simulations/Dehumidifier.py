from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
import simpy
import math
import copy
"""
Models a dehumidifier device that can absorb humidity from its environment.
"""


class Dehumidifier(DeviceComponentInterface):
    def __init__(self, water_tank_size, moisture_removal_efficiency, max_power, observer, env, location):
        """
        The dehumidifiers' constructor.
        Parameters
        ----------
        water_tank_size
            The size of the dehumidifiers water tank in [l]
        moisture_removal_efficiency
            The devices MRE in [g/J/s]
        max_power
            The maximum power of the dehumidifier
        observer
            The habitat observer
        env
            The SimPY environment the simulation runs in
        location
            The components' location
        """
        self.room = observer.habitat.find_room(location)
        self.max_power = max_power
        self.power = 0
        self.water_tank_size = water_tank_size # in litres
        self.moisture_removal_efficiency = moisture_removal_efficiency
        self.water_tank = simpy.Container(env, capacity=water_tank_size * 1000) #ToDo: Add Maintenance needed
        observer.dehumidifiers[location[0]][location[1]] = self
        super(Dehumidifier, self).__init__(1, "DEHUMIDIFIER", location, observer, env)

    def run(self, controller, habitat):
        """
        The main run process of the dehumidifier.
        Parameters
        ----------
        controller
            The control mechanism of the dehumidifier
        habitat
            The habitat the simulated devices "live" in.
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                self.power = self.max_power * self.controller.dehumidifier_map[self.location[0]][self.location[1]]
                if self.power > 0:
                    water_removed_per_duration = (self.moisture_removal_efficiency * self.power)/1000   # [g/ms]
                    time = math.ceil(1/water_removed_per_duration)
                    yield self.env.timeout(time)
                    yield self.room.humidity.get(1)
                    yield self.water_tank.put(1)
                else:
                    yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event
                self.power = 0

    def get_dummy(self):
        """
        Gets a dummy version of the dehumidifier for record keeping purposes.
        Returns
        -------
        dummy_dehumidifier:DummyDehumidifier
            A dummy dehumidifier used for history keeping purposes.

        """
        return DummyDehumidifier(self)


class DummyDehumidifier:
    """
    A dummy class used for record keeping purposes.
    """
    def __init__(self, dehumidifier):
        """
        The dummy dehumidifers' constructor
        Parameters
        ----------
        dehumidifier
            The dehumidifier the dummy is based on.
        """
        self.status = copy.copy(dehumidifier.status)
        self.max_power = copy.copy(dehumidifier.max_power)
        self.water_tank_size = copy.copy(dehumidifier.water_tank_size)
        self.water_tank_level = copy.copy(dehumidifier.water_tank.level)
        self.moisture_removal_efficiency = copy.copy(dehumidifier.moisture_removal_efficiency)
        self.power = copy.copy(dehumidifier.power)
        self.sub_process = False
