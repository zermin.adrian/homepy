import math
import sys

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface

"""
Abstract base class that models a component that can be connected to other components.
"""


class Connectable(DeviceComponentInterface):
    def __init__(self, bandwidth, transmission_error_probability, location, component_type, observer, env):
        """
        The connectables' constructor.
        Parameters
        ----------
        bandwidth
            Device Bandwidth in [bit/s]
        transmission_error_probability
            The error probability of transmission per bit.
        location
            The components' location
        component_type
            The component type
        observer
            The network observer
        env
            The SimPy environment the simulation runs in
        """
        self.min_packet_size = 21  # minimum package size (header + 1 bit)
        self.max_packet_size = 65535  # maximum package size of the communication protocol
        self.bandwidth = bandwidth / 1000  # transform bandwidth to [bit/ms]
        self.transmission_bit_error_probability = transmission_error_probability
        super(Connectable, self).__init__(1, component_type, location, observer, env)
        self.connection_id = self.add_connectable()
        self.destination_queue = []
        self.queue_length = 0
        self.current_package = None
        self.sub_process = None

    def add_connectable(self):
        """
        Function to add a new connectable.

        Returns
        -------
        id:int
            The connection ID of the added connectable.
        """
        self.observer.connectables.append(self)
        self.observer.shortest_path_times.append([sys.maxsize for s in range(len(self.observer.connectables))])
        for x in self.observer.signal_strengths:
            for y in x:
                y.append(0)
        for i in range(len(self.observer.connectables) - 1):
            self.observer.shortest_path_times[i].append(sys.maxsize)
        return self.observer.connectables.index(self)

    def run(self, controller, habitat):
        """
        The main process of the connectable.
        Parameters
        ----------
        controller
            The connectables' controller.
        habitat
            The habitat the simulated devices "live" in.
        """
        pass

    def reset(self):
        """
        Reset function of the connectable, gets called when component breaks.
        """
        pass

    def compute_send_delay(self, packet_size, signal_strength):
        """
        Calculates the send delay for a certain package.

        Parameters
        ----------
        packet_size
            The size of the package in bit.
        signal_strength
            The signal strength in [dbm]

        Returns
        -------
        ett:int
            Expected transfer time rounded up to whole [ms]
        """
        signal_noise_ratio = signal_strength / -90
        p = 1 - (1 - self.transmission_bit_error_probability) * (1 - self.transmission_bit_error_probability)
        # transmission bit error probability
        etx = 1 / (1 - p)
        capacity = math.log2(1 + signal_noise_ratio) + self.bandwidth
        ett = int(math.ceil(etx * (packet_size / capacity)))  # compute expected transmission time
        # return expected transmission time
        return ett
