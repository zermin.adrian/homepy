import copy
import simpy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Models a window shutter, reducing the light a window provides to a room.
"""


class Shutter(DeviceComponentInterface):
    def __init__(self, location, window, observer, env):
        """
        The shutters' constructor
        Parameters
        ----------
        location
            The location of the component
        window
            The window the shutter affects
        observer
            The responsible observer
        env
            The SimPy environment the simulation runs in
        """
        self.window = window
        observer.shutters[location[0]][location[1]] = self
        super(Shutter, self).__init__(1, "SHUTTER", location, observer, env)

    def run(self, controller, habitat):
        """
        The main process of the shutter.
        Parameters
        ----------
        controller
            The control mechanism of the shutter
        habitat
            The habitat the simulated devices "live" in
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                intended_shutter_factor = self.controller.shutter_map[self.location[0]][self.location[1]]
                if self.window.shutter_factor < intended_shutter_factor and self.window.shutter_factor < 1:
                    self.window.shutter_factor += 0.1
                elif self.window.shutter_factor > intended_shutter_factor and self.window.shutter_factor > 0:
                    self.window.shutter_factor -= 0.1
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def get_dummy(self):
        """
        Gets a dummy version of the shutter, used for record keeping purposes
        Returns
        -------
        dummy_shutter:DummyShutter
            The dummy version of the shutter
        """
        return DummyShutter(self)


class DummyShutter:
    """
    The dummy shutter class, used for record keeping purposes
    """
    def __init__(self, shutter):
        """
        The dummy shutters' constructor
        Parameters
        ----------
        shutter
            The shutter the dummy is based on
        """
        self.status = copy.copy(shutter.status)
        self.sub_process = False
