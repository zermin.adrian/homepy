import copy

import simpy

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface

"""
Models a smart lock that can be unlocked by entering a pin
"""


class Lock(DeviceComponentInterface):
    def __init__(self, pin, location, observer, env):
        """
        The constructor for a simulated lock
        Parameters
        ----------
        pin
            The pin to unlock the door
        location
            The components' location
        observer
            The current observer of the simulations
        env
            The SimPy environment the simulations runs in.
        """
        super(Lock, self).__init__(1, "LOCK", location, observer, env)
        self.pin = pin
        self.input = []
        self.opened = False
        self.open_event = simpy.events.Event(self.env)
        self.close_event = simpy.events.Event(self.env)
        self.open_event.callbacks.append(self.opening_starter)
        self.close_event.callbacks.append(self.closing_starter)
        self.observer.doors[self.location[0]][self.location[1]] = self

    def run(self, controller, habitat):
        """
        The run process for a simulated smart lock.
        Parameters
        ----------
        habitat
            The habitat the simulated devices "live" in
        controller
            The Controller mechanism controlling the smart lock
        """
        self.habitat = habitat
        self.controller = controller
        while True:
            try:
                yield self.env.timeout(1)
                self.input = []
            except simpy.Interrupt:
                self.opened = False
                self.input = []
                yield self.fixed_event

    def opening_starter(self, event):
        """
        Callback to start the opening process
        Parameters
        ----------
        event
            Event that triggered this callback
        """
        self.env.process(self.open())

    def open(self):
        """
        The locks opening process.
        """
        if self.input == self.pin:
            self.opened = True
            yield self.env.timeout(3)
            self.close_event.succeed(value="CLOSED")
            self.open_event = simpy.events.Event(self.env)
            self.open_event.callbacks.append(self.opening_starter)
        else:
            print("WRONG input")
        yield self.env.timeout(1)

    def closing_starter(self, event):
        """
        Callback to start the closing process
        Parameters
        ----------
        event
            Event that triggered this callback
        """
        self.env.process(self.close())

    def close(self):
        """
        Process to close the door.
        """
        yield self.env.timeout(2)
        self.close_event = simpy.events.Event(self.env)
        self.close_event.callbacks.append(self.closing_starter)
        self.opened = False

    def get_dummy(self):
        """
        Gets a dummy version a simulated lock
        Returns
        -------
        dummy_lock:DummyLock
            The dummy version of the lock used for record keeping purposes
        """
        return DummyLock(self)


class DummyLock:
    """
    Dummy class used for record keeping purposes
    """
    def __init__(self, lock):
        """
        The dummy locks' constructor
        Parameters
        ----------
        lock
            The lock the dummy is based on
        """
        self.opened = copy.copy(lock.opened)
        self.status = copy.copy(lock.status)
