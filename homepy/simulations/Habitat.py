import simpy
import copy
import math

DAYLIGHT_LUX = 10000
DENSITY_OF_AIR = 1225  # in g/m³ at 15 degrees celsius and 1013.25 hPa

A = 6.116441  # Constants for calculating saturation_vapour_pressure
M = 7.591386
TN = 240.7263

C = 2.16679  # gK/J constant for calculating absolute humidity
K = 273.13  # Kelvin temperature constant


def get_saturation_vapour_pressure(temperature):
    """
    Calculates the saturation vapour pressure, based on the temperature
    Parameters
    ----------
    temperature
        The current temperature in degree celsius
    Returns
    -------
    saturation_vapour_pressure
        The saturation vapour pressure
    """
    return A * math.pow(10, (M * temperature) / (temperature + TN))


def get_abs_humidity(temperature, vapour_pressure):
    """
    Calculates absolute humidity in [g/m³] based on temperature and vapour pressure.
    Parameters
    ----------
    temperature
        The current temperature in degree celsius
    vapour_pressure
        The current vapour pressure.
    Returns
    -------
    abs_humidity:
        The absolute humidity
    """
    return C * 100 * vapour_pressure / (K + temperature)


def get_vapour_pressure(relative_humidity, saturation_vapour_pressure):
    """
    Calculates the vapour pressure based on the relative humidity and the saturation vapour pressure.
    Parameters
    ----------
    relative_humidity:int
        The relative humidity in %.
    saturation_vapour_pressure
        The saturation vapour pressure

    Returns
    -------
    vapour_pressure:
        The vapour pressure
    """
    return relative_humidity / 100 * saturation_vapour_pressure


class Habitat:
    """
    Models a general smart home environment with walls, rooms, humidity, temperature and lumen.
    """
    def __init__(self, map_size, outside_temperature, outside_humidity, env):
        """
        The habitats constructor, initializes an example house map.
        Parameters
        ----------
        map_size
            The size of the simulated smart home area
        outside_temperature
            The outside temperature and inside starting temperature
        outside_humidity
            The outside humidity and inside starting humidity
        env
            The SimPy environment the simulation runs in
        """
        self.map_size = map_size
        self.map = self.get_empty_map()
        self.create_example_house_map(self.map)
        self.env = env
        self.rooms = []
        self.outside_temperature = outside_temperature
        self.outside_humidity = outside_humidity
        self.init_rooms()

    def get_relative_humidity(self, location):
        """
        Finds the relative humidity based on the location.
        Parameters
        ----------
        location
            The location where the humidity should be measured

        Returns
        -------
        relative_humidity:int
            The relative humidity in %
        """
        room = self.find_room(location)
        return room.relative_humidity

    def get_temperature(self, location):
        """
        Finds the temperature at a certain location
        Parameters
        ----------
        location
            The location where the temperature should be measured
        Returns
        -------
        temperature:int
            The temperature in degree celsius
        """
        room = self.find_room(location)
        return room.temperature

    def get_luminosity(self, location):
        """
        Finds the amount of light at a certain location.
        Parameters
        ----------
        location
            The location where the light should be measured
        Returns
        -------
        illumination:
            The light measured in lux
        """
        room = self.find_room(location)
        return room.illumination

    def find_room(self, location):
        """
        Finds the room the given location is part of
        Parameters
        ----------
        location
            The location for which the room has to be found
        Returns
        -------
        room:Room
            The room the location is a part of
        """
        for room in self.rooms:
            if location in room.coordinates:
                return room
        return None

    def init_rooms(self):
        """
        Initializes the rooms based on the wall structure.
        """
        map_copy = [[copy.copy(self.map[x_coordinate][y_coordinate]) for y_coordinate in range(len(self.map[x_coordinate]))] for
                    x_coordinate in range(len(self.map))]
        # create a copy of map
        rooms = []
        for x_coordinate in range(len(map_copy)):
            for y_coordinate in range(len(map_copy[x_coordinate])):
                if map_copy[x_coordinate][y_coordinate] == 0 and map_copy[x_coordinate - 1][y_coordinate] == 1 and \
                        map_copy[x_coordinate][y_coordinate - 1] == 1 and \
                        map_copy[x_coordinate - 1][y_coordinate - 1] == 1:
                    rooms.append(self.get_room_horizontal([x_coordinate, y_coordinate], map_copy))
        for room in rooms:
            created_room = Room(self.outside_temperature, self.outside_humidity, room, self.env, self)
            self.rooms.append(created_room)
            created_room.id = self.rooms.index(created_room)
            self.env.process(created_room.run())

    def get_room_horizontal(self, coordinate, map):
        """
        Creates a room based on the wall structure with a top left corner of a room.
        Parameters
        ----------
        coordinate
            THe top left corner of the room
        map
            The map of the smart home, with 1s as walls
        Returns
        -------
        room:Room
            The created room
        """
        room = []
        for x_coordinate in range(len(map)):
            if map[coordinate[0] + x_coordinate][coordinate[1]]:
                break
            else:
                room.append([coordinate[0] + x_coordinate, coordinate[1]])
        length = self.get_length(room)
        next_location = self.get_next_location(map, length, coordinate)
        if next_location is not None:
            next = self.get_room_horizontal([next_location[0], next_location[1]], map)
            room.extend(next)
        else:
            return room
        return room

    def get_length(self, room):
        """
        Gets the horizontal length of a given room
        Parameters
        ----------
        room:list
            The room
        Returns
        -------
        length:int
            The length of a given room
        """
        upper = 0
        lower = len(self.map)
        for location in room:
            if location[0] > upper:
                upper = location[0]
            if location[0] < lower:
                lower = location[0]
        return upper - lower

    @staticmethod
    def get_next_location(map, length, location):
        """
        Returns the next location of the next horizontal line to be added to a room
        Parameters
        ----------
        map
            The smart home map
        length
            The length of the room
        location
            The current location

        Returns
        -------
        location:list
            The next location
        """
        for i in range(length):
            if not map[location[0] + i][location[1] + 1]:
                return [location[0] + i, location[1] + 1]
        return None

    @staticmethod
    def create_example_house_map(map):
        """
        Creates an example house for simulation purposes
        """
        for x in range(len(map)):
            for y in range(len(map[x])):
                if x == 20 and y in range(20, 80):  # create four walls
                    map[x][y] = 1
                if x in range(20, 80) and y == 20:
                    map[x][y] = 1
                if x in range(20, 80) and y == 80:
                    map[x][y] = 1
                if x == 80 and y in range(20, 80):
                    map[x][y] = 1
                if x in range(20, 80) and y == 50:
                    map[x][y] = 1
                if x == 45 and y in range(20, 70):
                    map[x][y] = 1
                if x in range(45, 80) and y == 70:
                    map[x][y] = 1
                if x == 35 and y in range(50, 80):
                    map[x][y] = 1

    def get_empty_map(self):
        """
        Returns an empty map with the correct map size
        Returns
        -------
        map:list
            The created map
        """
        return [[0 for g in range(self.map_size)] for h in range(self.map_size)]


class Window:
    """
    Models a window
    """
    def __init__(self, size, location, observer, env):
        """
        The modeled windows constructor
        Parameters
        ----------
        size
            The windows size in m²
        location
            The windows location
        observer
            The habitat observer
        env
            The SimPy environment the simulation runs in
        """
        self.size = size
        self.env = env
        self.room = observer.habitat.find_room(location)
        self.status = "CLOSED"
        self.shutter_factor = 1
        self.open_event = simpy.Event(self.env)
        self.open_event.callbacks.append(self.open_window_trigger)
        self.close_event = simpy.Event(self.env)
        self.close_event.callbacks.append(self.close_window)
        self.processes = []

    def run(self):
        """
        The run process of the window, adding light to a room.
        """
        while True:
            provided_light = DAYLIGHT_LUX * self.size
            self.room.illumination += (provided_light / (len(self.room.coordinates)/25)) * self.shutter_factor
            # [lx or lm/m²]
            yield self.env.timeout(1)

    def open_window_trigger(self, event):
        """
        Event callback, triggered when a window gets opened, begins an open window process
        Parameters
        ----------
        event
            The event that triggered the callback
        """
        self.status = "OPEN"
        self.processes.append(self.env.process(self.temperate_room()))
        self.processes.append(self.env.process(self.humidity_exchange()))
        self.close_event = simpy.Event(self.env)
        self.close_event.callbacks.append(self.close_window)

    def close_window(self, event):
        """
        Closes the window, callback to end all open window processes
        Parameters
        ----------
        event
            The event that triggered the callback
        """
        self.status = "CLOSED"
        for process in self.processes:
            if process.is_alive:
                process.interrupt()
        self.open_event = simpy.Event(self.env)
        self.open_event.callbacks.append(self.open_window_trigger)

    def temperate_room(self):
        """
        Models the temperature exchange process when a window gets opened.
        """
        try:
            while True:
                temp_diff = self.room.temperature - self.room.habitat.outside_temperature
                total_time = int(abs(self.room.volume * temp_diff))
                for i in range(total_time):
                    if temp_diff > 0:
                        self.room.temperature += 1
                        yield self.env.timeout(1)
                    else:
                        self.room.temperature -= 1
                        yield self.env.timeout(1)
                yield self.env.timeout(1)
        except simpy.Interrupt:
            print("Window was closed")

    def humidity_exchange(self):
        """
        Models the humidity exchange process of an opened window.
        """
        outside_temperature = self.room.habitat.outside_temperature
        outside_humidity = self.room.habitat.outside_humidity
        outside_abs_humidity = get_abs_humidity(outside_temperature, get_vapour_pressure(outside_humidity,
                                                                                         get_saturation_vapour_pressure(
                                                                                             outside_temperature)))
        inside_abs_humidity = self.room.absolute_humidity
        humidity_difference = outside_abs_humidity - inside_abs_humidity
        try:
            while True:
                if humidity_difference > 0:
                    self.room.humidity.put(humidity_difference)
                elif humidity_difference < 0:
                    self.room.humidity.get(abs(humidity_difference))
                yield self.env.timeout(1)
                humidity_difference = get_abs_humidity(outside_temperature, get_vapour_pressure(outside_humidity,
                                                                                                get_saturation_vapour_pressure(
                                                                                                outside_temperature))) - self.room.absolute_humidity
        except simpy.Interrupt:
            print("Window was closed")

    def get_dummy(self):
        """
        Gets a dummy window used for history purposes
        Returns
        -------
        dummy_window:DummyWindow
            A dummy version of the window
        """
        return DummyWindow(self)


class DummyWindow:
    """
    Dummy window class, used for record keeping purposes
    """
    def __init__(self, window):
        """
        Constructor of the dummy window.
        Parameters
        ----------
        window:Window
            The window the dummy is based on.
        """
        self.status = copy.copy(window.status)
        self.size = copy.copy(window.size)
        self.shutter_factor = copy.copy(window.shutter_factor)


class Room:
    """
    Models an ideal closed room in a house.
    """
    def __init__(self, temperature, starting_humidity, coordinates, env, habitat):
        self.habitat = habitat
        self.id = 0
        self.env = env
        self.coordinates = coordinates

        self.volume = (len(self.coordinates) / 25) * 2  # [m³]
        self.air_mass = DENSITY_OF_AIR * self.volume    # [g]

        self.temperature = temperature                  # [Celsius]
        self.relative_humidity = starting_humidity      # [%]
        self.saturation_vapour_pressure = get_saturation_vapour_pressure(temperature)   # [hPa]
        self.vapour_pressure = get_vapour_pressure(self.relative_humidity, self.saturation_vapour_pressure)
        self.absolute_humidity = get_abs_humidity(self.temperature, self.vapour_pressure)   # [g/m³]
        self.humidity = simpy.Container(self.env, init=self.absolute_humidity * self.volume)
        # Container with grams of water
        self.illumination = 0

    def run(self):
        """
        The main simulation process of the room
        """
        while True:
            self.relative_humidity = math.ceil(self.get_relative_humidity_from_container())
            self.saturation_vapour_pressure = get_saturation_vapour_pressure(self.temperature)
            self.vapour_pressure = get_vapour_pressure(self.relative_humidity, self.saturation_vapour_pressure)
            self.absolute_humidity = get_abs_humidity(self.temperature, self.vapour_pressure)
            yield self.env.timeout(1)
            self.illumination = 0

    def heat_up(self, power, temperature):
        """
        Heat up process of a room
        Parameters
        ----------
        power
            Power with which the room gets heated up
        temperature
            The temperature of the heating unit
        """
        try:
            joule_per_time_unit = power / 1000
            time = math.ceil(self.air_mass / joule_per_time_unit)
            yield self.env.timeout(time)
            if temperature > self.temperature:
                self.temperature += 1
        except simpy.Interrupt:
            print("Heating Process interrupted")

    def cool_down(self, power, max_power, max_btu_per_hour):
        """
        Cool down process of a room.
        Parameters
        ----------
        power
            Power the cooling unit has
        max_power
            The maximum possible power of the cooling unit
        max_btu_per_hour
            Maximum BTU absorbed by the cooling unit
        """
        try:
            max_absorbed_joule_per_hour = max_btu_per_hour/1055                         # [J/h]
            joule_per_time_unit = ((max_absorbed_joule_per_hour) / (60 * 60 * 1000))    # [J/ms]
            time = self.air_mass / ((power / max_power) * joule_per_time_unit)          # [ms]
            yield self.env.timeout(time)
            self.temperature -= 1
        except simpy.Interrupt:
            print("cooling process interrupted")

    def get_relative_humidity_from_container(self):
        """
        Calculates the current relative humidity of the room
        """
        return ((self.humidity.level / self.volume) * (self.temperature + K)) / \
               (C * get_saturation_vapour_pressure(self.temperature))
