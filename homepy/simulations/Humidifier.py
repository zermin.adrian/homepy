import simpy
import math
import copy
from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface

"""
Models a humidifier component that humidifies its environment
"""


class Humidifier(DeviceComponentInterface):
    def __init__(self, water_tank_size, vaporisation_duration, observer, env, location):
        """
        The humidifiers' constructor
        Parameters
        ----------
        water_tank_size
            The water tank size of the humidifier in litres [l]
        vaporisation_duration
            The time it takes the component to turn the water into water vapour in hours [h]
        observer
            The habitat observer
        env
            The SimPy environment the simulations runs in
        location
            The components' location
        """
        self.room = observer.habitat.find_room(location)
        self.water_tank_size = water_tank_size  # in litres
        self.vaporisation_duration = vaporisation_duration  # in hours
        self.water_tank = simpy.Container(env, capacity=water_tank_size * 1000)
        self.grams_of_water_per_duration = (water_tank_size * 1000)/(vaporisation_duration*60*60*1000)  # [g/ms]
        observer.humidifiers[location[0]][location[1]] = self
        super(Humidifier, self).__init__(1, "HUMIDIFIER", location, observer, env)

    def run(self, controller, habitat):
        """
        The main process of the humidifier
        Parameters
        ----------
        controller
            The humidifiers control mechanism
        habitat
            The habitat the simulated devices "live" in.
        """
        self.controller = controller
        self.habitat = habitat
        while True:
            try:
                if self.controller.humidifier_map[self.location[0]][self.location[1]]:
                    time = math.ceil(1000 / self.grams_of_water_per_duration)
                    yield self.env.timeout(time)
                    yield self.water_tank.get(1)
                    yield self.room.humidity.put(1)
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def get_dummy(self):
        """
        Gets a dummy version of the humidifier, used for record keeping purposes
        """
        return DummyHumidifier(self)


class DummyHumidifier:
    """
    The dummy class of the humidifier, used for record keeping purposes
    """
    def __init__(self, humidifier):
        """
        The dummy humidifers' constructor
        Parameters
        ----------
        humidifier
            The humidifier the dummy is based on
        """
        self.status = copy.copy(humidifier.status)
        self.water_tank_level = copy.copy(humidifier.water_tank.level)
        self.water_tank_size = copy.copy(humidifier.water_tank_size)
        self.vaporisation_duration = copy.copy(humidifier.vaporisation_duration)
