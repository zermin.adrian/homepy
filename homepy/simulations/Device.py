import simpy
import copy

from homepy.simulations.Dislocator import Dislocator

"""
Models a breakable smart home device
"""


class Device:
    def __init__(self, habitat, env, location, components, security_controller, routing_controller, habitat_controller,
                 observer, priority):
        """
        The devices' constructor.
        Parameters
        ----------
        habitat
            The habitat the simulated devices "live" in.
        env
            The SimPy environment the simulation runs in.
        location:list
            The devices' location.
        components:list
            The components of the device
        security_controller
            The simulations' security controller
        routing_controller
            The simulations' network controller
        habitat_controller
            The simulations' environmental controller
        observer
            The device health observer
        priority
            The devices' priority, given by the user
        """
        self.env = env
        self.habitat = habitat
        self.habitat_controller = habitat_controller
        self.security_controller = security_controller
        self.routing_controller = routing_controller
        self.location = location
        self.components = components
        self.observer = observer
        self.observer.devices.append(self)
        self.status = 1
        self.break_event = simpy.events.Event(self.env)
        self.break_event.callbacks.append(self.break_device)
        self.fixed_event = simpy.events.Event(self.env)
        self.fixed_event.callbacks.append(self.fix_device)
        self.componentProcesses = []
        self.process = self.env.process(self.run())
        self.priority = priority
        self.process_dict = {   # Assign the right controller to the devices' components
            "AIRCOOLER": self.habitat_controller,
            "ALARM": self.security_controller,
            "CAMERA": self.security_controller,
            "CLIENT": self.routing_controller,
            "DEHUMIDIFIER": self.habitat_controller,
            "DISLOCATOR": self.security_controller,
            "GATEWAY": self.routing_controller,
            "HEATER": self.habitat_controller,
            "HUMIDIFIER": self.habitat_controller,
            "LIGHTBULB": self.habitat_controller,
            "LUMINOSITYSENSOR": self.habitat_controller,
            "LOCK": self.security_controller,
            "THERMOMETER": self.security_controller,
            "THERMOSTAT": self.habitat_controller,
            "WINDOWOPENER": self.habitat_controller,
            "MOTIONSENSOR": self.security_controller,
            "MOTOR": self.security_controller,
            "ROTATOR": self.security_controller,
            "ROUTER": self.routing_controller,
            "SHUTTER": self.habitat_controller
        }

    def run(self):
        """
        Run process of the device, keeps updating its own location and waiting to be broken.
        """
        for i in self.components:
            i.process = self.env.process(i.run(self.process_dict.get(i.type), self.habitat))
        while True:
            try:
                for i in self.components:
                    if isinstance(i, Dislocator) and i.location != self.location:
                        self.habitat.map[self.location[0]][self.location[1]] = 0
                        self.location = i.location
                        self.habitat.map[self.location[0]][self.location[1]] = self
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event

    def broken(self):
        """
        Broken process of the device, waiting to be fixed.
        """
        yield self.fixed_event  # Wait for device to be fixed
        self.break_event = simpy.events.Event(self.env)
        self.break_event.callbacks.append(self.break_device)

    def break_device(self, event):
        """
        Callback function that starts the broken process of the device.
        Parameters
        ----------
        event
            Event that triggered the callback
        """
        self.process.interrupt()
        self.status = 0
        for i in self.components:
            i.break_event.succeed(value="BREAK")
        self.env.process(self.broken())     # Start new process of broken device

    def fix_device(self, event):
        """
        Function that gets called when a device has been fixed, triggers fixed events for its components
        Parameters
        ----------
        event
            Event that triggered this callback
        """
        self.status = 1
        for i in self.components:
            i.fixed_event.succeed(value="FIXED")    # Trigger all fixed events of components
        self.fixed_event = simpy.events.Event(self.env)
        self.fixed_event.callbacks.append(self.fix_device)

    def get_dummy(self):
        """
        Returns a dummy version of the device, used for record keeping purposes.
        Returns
        -------
        dummy_device:DummyDevice
            The dummy device, used for record keeping purposes
        """
        return DummyDevice(self)


class DummyDevice:
    """
    Dummy device class used for record keeping purposes.
    """
    def __init__(self, device):
        """
        Constructor for the dummy device.
        Parameters
        ----------
        device:Device
            The device the dummy is based on.
        """
        self.status = copy.copy(device.status)
        self.priority = copy.copy(device.priority)
