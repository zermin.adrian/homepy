import copy

from homepy.simulations.DeviceComponentInterface import DeviceComponentInterface
"""
Base class for modeling any sensor.
"""


class Sensor(DeviceComponentInterface):
    def __init__(self, direction, shape, dimension, location, component_type, client, observer, env):
        """
        Constructor for the sensor class.
        Parameters
        ----------
        direction
            Direction the sensor is pointing in.
        shape
            shape of the sensor area.
        dimension
            The size or range of the sensed area.
        location
            The sensors' location
        component_type
            The type of the component
        client
            The client that is sending sensory data to the intended gateway
        observer
            The current observer of the simulations
        env
            The simPy environment the sensor runs in
        """
        super(Sensor, self).__init__(1, component_type, location, observer, env)
        self.direction = direction
        self.shape = shape
        self.dimension = dimension
        self.sensor_area = SensorArea(direction, location, shape, dimension, self.observer.habitat)
        self.intended_sensor_area = self.sensor_area
        self.client = client
        self.current = None

    def run(self, controller, priority):
        pass

    def sense(self):
        pass

    def compute_sensor_area(self):
        pass

    def get_dummy(self):
        return DummySensor(self)

    def reset(self):
        pass

    @staticmethod
    def convert_to_binary(sensor_array):
        pass


class SensorArea:
    """
    Sensor area class, used for modeling the area on which sensors can perceive their environment
    """
    def __init__(self, direction, location, shape, dimension, habitat):
        """
        The sensor areas' constructor
        Parameters
        ----------
        direction
            The direction the sensor is facing in
        location
            The sensors' location
        shape
            The shape of the sensed area
        dimension
            The range of the sensor
        habitat
            The habitat the simulated devices "live" in
        """
        self.habitat = habitat
        self.direction = direction
        self.location = location
        self.shape = shape
        self.dimension = dimension
        self.coordinates = self.get_coordinates()

    def get_coordinates(self):
        """
        Calculates the coordinates of the perceived area
        Returns
        -------
        coordinates:list
            The coordinates of the sensed area
        """
        length = self.dimension
        if length:
            shape_dict = {
                "SELF": [self.location],
                "SQUARE": self.get_square(length),
                "TRIANGLE": self.get_triangle(length),
                "LINE": self.get_line(length)
            }
        else:
            return []
        coordinates = shape_dict.get(self.shape)
        self.remove_out_of_map(coordinates)
        self.remove_behind_walls(coordinates)
        return coordinates

    def get_line(self, length):
        """
        Gets the coordinates of a line shaped area with the given length
        Parameters
        ----------
        length
            The range of the sensor

        Returns
        -------
        coordinates:list
            The coordinates in the shape of a line
        """
        coordinates = []
        for i in range(length):
            if self.direction == "N":
                coordinates.append([self.location[0] - i, self.location[1]])
            if self.direction == "NE":
                coordinates.append([self.location[0] - i, self.location[1] + i])
            if self.direction == "E":
                coordinates.append([self.location[0], self.location[1] + i])
            if self.direction == "SE":
                coordinates.append([self.location[0] + i, self.location[1] + i])
            if self.direction == "S":
                coordinates.append([self.location[0] + i, self.location[1]])
            if self.direction == "SW":
                coordinates.append([self.location[0] + i, self.location[1] - i])
            if self.direction == "W":
                coordinates.append([self.location[0], self.location[1] - i])
            if self.direction == "NW":
                coordinates.append([self.location[0] - i, self.location[1] - i])
        return coordinates

    def get_square(self, length):
        """
        Gets the coordinates of a square shaped area with the given length
        Parameters
        ----------
        length
            The range of the sensor

        Returns
        -------
        coordinates:list
            The coordinates in the shape of a square
        """
        coordinates = []
        for i in range(length):
            for j in range(length):
                top_right = [self.location[0] - i, self.location[1] + j]
                low_left = [self.location[0] + i, self.location[1] - j]
                low_right = [self.location[0] + i, self.location[1] + j]
                top_left = [self.location[0] - i, self.location[1] - j]
                if top_right not in coordinates:
                    coordinates.append(top_right)
                if low_left not in coordinates:
                    coordinates.append(low_left)
                if low_right not in coordinates:
                    coordinates.append(low_right)
                if top_left not in coordinates:
                    coordinates.append(top_left)

    def get_triangle(self, length):
        """
        Gets the coordinates of a triangle shaped area with the given length
        Parameters
        ----------
        length
            The range of the sensor

        Returns
        -------
        coordinates:list
            The coordinates in the shape of a triangle
        """
        coordinates = [self.location]
        if self.direction == "N":
            for i in range(1, self.dimension):
                for j in range(0, self.dimension):
                    top_left = [self.location[0] - i, self.location[1] - j]
                    top_right = [self.location[0] - i, self.location[1] + j]
                    if top_right not in coordinates:
                        coordinates.append(top_right)
                    if top_left not in coordinates:
                        coordinates.append(top_left)
        if self.direction == "E":
            for i in range(1, self.dimension):
                for j in range(0, self.dimension):
                    top_right = [self.location[0] - i, self.location[1] + j]
                    low_right = [self.location[0] + i, self.location[1] + j]
                    if top_right not in coordinates:
                        coordinates.append(top_right)
                    if low_right not in coordinates:
                        coordinates.append(low_right)
        if self.direction == "S":
            for i in range(1, self.dimension):
                for j in range(0, self.dimension):
                    low_right = [self.location[0] + i, self.location[1] + j]
                    low_left = [self.location[0] + i, self.location[1] - j]
                    if low_left not in coordinates:
                        coordinates.append(low_left)
                    if low_right not in coordinates:
                        coordinates.append(low_right)
        if self.direction == "W":
            for i in range(1, self.dimension):
                for j in range(0, self.dimension):
                    low_left = [self.location[0] + i, self.location[1] - j]
                    top_left = [self.location[0] - i, self.location[1] - j]
                    if low_left not in coordinates:
                        coordinates.append(low_left)
                    if top_left not in coordinates:
                        coordinates.append(top_left)
        return coordinates

    def remove_behind_walls(self, coordinates):
        """
        Removes all coordinates that cant be perceived because they are behind walls.
        Parameters
        ----------
        coordinates:list
            The coordinates of the sensed area
        """
        center = self.location
        to_be_deleted = []
        if self.shape == "LINE":
            for location in coordinates:
                if self.habitat.map[location[0]][location[1]] == 1:
                    diffx = center[0] - location[0]
                    diffy = center[1] - location[1]
                    if diffx > 0 and diffy > 0:
                        for i in coordinates:
                            if center[0] - i[0] >= diffx and center[1] - i[1] >= diffy:
                                to_be_deleted.append(i)
                    if diffx == 0 and diffy > 0:
                        for i in coordinates:
                            if center[0] - i[0] == diffx and center[1] - i[1] >= diffy:
                                to_be_deleted.append(i)
                    if diffx > 0 and diffy == 0:
                        for i in coordinates:
                            if center[0] - i[0] >= diffx and center[1] - i[1] == diffy:
                                to_be_deleted.append(i)
                    if diffx < 0 and diffy < 0:
                        for i in coordinates:
                            if center[0] - i[0] <= diffx and center[1] - i[1] <= diffy:
                                to_be_deleted.append(i)
                    if diffx == 0 and diffy < 0:
                        for i in coordinates:
                            if center[0] - i[0] == diffx and center[1] - i[1] <= diffy:
                                to_be_deleted.append(i)
                    if diffx < 0 and diffy == 0:
                        for i in coordinates:
                            if center[0] - i[0] <= diffx and center[1] - i[1] == diffy:
                                to_be_deleted.append(i)
                    if diffx < 0 < diffy:
                        for i in coordinates:
                            if center[0] - i[0] <= diffx and center[1] - i[1] >= diffy:
                                to_be_deleted.append(i)
                    if diffx > 0 > diffy:
                        for i in coordinates:
                            if center[0] - i[0] >= diffx and center[1] - i[1] <= diffy:
                                to_be_deleted.append(i)
                                # ToDo: Add Triangle and Square cases
        for element in to_be_deleted:
            if element in coordinates:  # ToDo: Find more elegant solution
                coordinates.remove(element)

    @staticmethod
    def remove_out_of_map(coordinates):
        """
        Removes all coordinates that aren't on the map
        Parameters
        ----------
        coordinates
            The coordinate array
        """
        to_be_deleted = []
        for location in coordinates:
            if location[0] >= 100 or location[1] >= 100 or location[0] < 0 or location[1] < 0:
                to_be_deleted.append(location)
        for element in to_be_deleted:
            coordinates.remove(element)


class DummySensor:
    """
    Dummy class of the sensor, used for record keeping purposes
    """
    def __init__(self, sensor):
        """
        The dummy sensors' constructor
        Parameters
        ----------
        sensor:Sensor
            The sensor the dummy is based on
        """
        self.type = copy.copy(sensor.type)
        self.sensor_area = copy.deepcopy(sensor.sensor_area.coordinates)
        self.direction = copy.copy(sensor.direction)
        self.status = copy.copy(sensor.status)
        self.priority = copy.copy(sensor.habitat)
        self.current = copy.copy(sensor.current)
        self.sub_process = sensor.process is not None
