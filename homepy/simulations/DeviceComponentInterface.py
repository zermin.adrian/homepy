import simpy

"""
Abstract base class for device components that can break.
"""


class DeviceComponentInterface:
    def __init__(self, status, type, location, observer, env):
        """
        The device components constructor.
        Parameters
        ----------
        status
            Status of the component, where 0 is broken, 1 is working normally
        type
            Component type
        location
            The components' location
        observer
            The current observer of the simulations
        env
            The SimPy environment of the simulations
        """
        self.observer = observer
        self.status = status
        self.type = type
        self.location = location
        self.env = env
        self.break_event = simpy.events.Event(self.env)     # Event that can be triggered to break a device
        self.fixed_event = simpy.events.Event(self.env)     # Event that can be triggered to fix a device
        self.break_event.callbacks.append(self.break_component)
        self.fixed_event.callbacks.append(self.fix_component)
        self.habitat = 1
        self.process = None

    def run(self, controller, habitat):
        """
        The main process of the component.

        Parameters
        ----------
        controller
            The control mechanism of the component
        habitat
            The habitat the simulated devices "live" in.
        """
        pass

    def broken(self):
        """
        Method that is triggered when a component is broken. It stays broken until it gets fixed.
        """
        self.status = 0
        yield self.fixed_event
        self.break_event = simpy.events.Event(self.env)
        self.break_event.callbacks.append(self.break_component)

    def break_component(self, event):
        """
        Method that starts the simulated process of a broken component.
        Parameters
        ----------
        event
            Event that triggered was triggered to break the device.
        """
        self.env.process(self.broken())

    def fix_component(self, event):
        """
        Method that gets called once a component gets broken.
        Parameters
        ----------
        event
            Event that was triggered to fix the device
        """
        self.status = 1
        self.fixed_event = simpy.events.Event(self.env)
        self.fixed_event.callbacks.append(self.fix_component)
