import simpy

from homepy.simulations.Connectable import Connectable
from homepy.simulations.DataPackage import DataPackage

"""
Models a network client that generates data packages and sends them to a destination
"""


class Client(Connectable):
    def __init__(self, destination, location, observer, env):
        """
        Constructor of the client

        Parameters
        ----------
        destination
            The destination of the clients' packages.
        location
            The clients' location.
        observer
            The network observer.
        env
            The SimPy environment the simulations runs in
        """
        self.destination = destination
        super(Client, self).__init__(640000000, 0.98, location, "CLIENT", observer, env)
        self.to_be_sent = simpy.Container(self.env, 1000000000)  # Bytes to be sent, max 1GB storage

    def run(self, controller, habitat):
        """
        The clients' run function. It generates data packages in certain intervals and sends them towards their
        destination according to the routing table.
        Parameters
        ----------
        habitat
            The Overall device habitat, added by the user himself
        controller
            The controller mechanism that provides the client with a routing table
        """
        self.habitat = habitat
        self.learning = controller
        while True:
            try:
                self.current_package = None
                if self.to_be_sent.level:
                    self.sub_process = self.env.process(self.generate_package(self.destination))
                    data_package = yield self.sub_process
                    self.current_package = data_package

                    print("Data_Package Size:" + str(data_package.size))
                    while self.learning.routing_table[self.connection_id][self.destination.connection_id] is None:
                        yield self.env.timeout(1)
                    sending_to = self.observer.connectables[
                        int(self.learning.routing_table[self.connection_id][self.destination.connection_id])]
                    send_delay = self.compute_send_delay(data_package.size,
                                                         self.observer.signal_strengths[self.location[0]][self.location[1]][sending_to.connection_id])
                    data_package.next = sending_to
                    data_package.routing_event.succeed(value="ROUTING")
                    self.sub_process = self.env.process(self.send_package(data_package, send_delay))
                    yield self.sub_process
                    self.sub_process = None
                yield self.env.timeout(1)
            except simpy.Interrupt:
                yield self.fixed_event
                if self.sub_process:
                    if self.sub_process.is_alive:
                        self.sub_process.interrupt()
                for j in self.observer.packages:
                    if j.current == self:
                        self.observer.packages.remove(j)
                        j.lost_event.succeed()
                self.destination_queue = []
                self.current_package = None
                self.to_be_sent = simpy.Container(self.env, 1000000000)  # Bytes to be sent, max 1GB storage

    def send_package(self, data_package, send_delay):
        try:
            yield self.env.timeout(send_delay)
            data_package.next.destination_queue.append(data_package.destination.connection_id)
            data_package.next.connection.put(data_package)
            data_package.sent_event.succeed(value="SENT")
            data_package.current = data_package.next
        except simpy.Interrupt:
            print("Package sending process interrupted")

    def generate_package(self, destination):
        """
        Generates a data package.
        Parameters
        ----------
        destination
            The intended destination of the data package.

        Returns
        -------
        dp : DataPackage
            The generated data package.

        """
        if self.to_be_sent.level > self.max_packet_size - 20:
            size = self.max_packet_size - 20
        else:
            size = self.to_be_sent.level
        dp = DataPackage(self.env, self, self, size, destination, self.observer)
        dp.load = yield self.to_be_sent.get(size)
        dp.path.append(self.connection_id)
        self.env.process(dp.run())
        return dp
