import random

REPAIR_POSSIBILITY = 0.9
SUCCESSFUl_REPAIR_TIME = 6


class Repairman:
    def __init__(self, env, name, observer):
        self.observer = observer
        self.name = name
        self.env = env  # simpy environment the repairman is working in
        self.busy = 0
        self.subject = None  # Device that is getting repaired
        self.time_left = 0
        self.observer.repairmen.append(self)
        self.broken_units = []

    def repair(self, device):  # repair process
        self.busy = 1
        self.subject = device
        for i in device.components:
            if not i.status:
                self.broken_units.append(i)
        self.time_left = SUCCESSFUl_REPAIR_TIME
        for i in range(SUCCESSFUl_REPAIR_TIME):
            self.time_left -= 1
            yield self.env.timeout(1)
        device.status = 1
        if not device.fixed_event.triggered:
            device.fixed_event.succeed(value=str(self.name))
        self.busy = 0
        self.time_left = 0
        self.subject = None
        self.broken_units = []

    def compute_repair_time(self, broken_units):
        pos = random.randint(1, 10) / 10
        if len(broken_units) > 0:
            if pos < REPAIR_POSSIBILITY:
                del broken_units[0]
                return SUCCESSFUl_REPAIR_TIME + self.compute_repair_time(broken_units)
            else:
                return SUCCESSFUl_REPAIR_TIME + self.compute_repair_time(broken_units)
        return 0

    def get_dummy(self):
        return DummyRepairman(self)

class DummyRepairman:
    def __init__(self, repairman):
        self.busy = repairman.busy
        self.time_left = repairman.time_left
