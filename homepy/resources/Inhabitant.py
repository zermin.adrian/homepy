import random

from homepy.simulations.Camera import Camera
from homepy.simulations.Device import Device
from homepy.simulations.Dislocator import Dislocator
from homepy.simulations.MotionSensor import MotionSensor
from homepy.simulations.Motor import Motor
from homepy.simulations.Rotator import Rotator

PIN = 1234


class Inhabitant:
    def __init__(self, env,  probability_to_break, name, observer):
        self.break_seed = random.uniform(0, 1)
        self.leave_seed = random.randint(1, 3)
        self.probability_to_break = probability_to_break
        self.observer = observer
        self.env = env
        self.name = name
        self.time_to_add = 5

    def run(self, learning):
        self.learning = learning
        while True:
            for i in self.observer.devices:
                if i.status:
                    if self.break_seed <= self.probability_to_break:
                        i.break_event.succeed(value=self.name)
                self.break_seed = random.uniform(0, 1)
            # ToDo: Make Inhabitant open doors
            yield self.env.timeout(1)
