import copy


class SecurityHistory:

    def __init__(self):
        self.sensor_state_history = []
        self.security_reward_history = []
        self.total_motion_sensor_area = []
        self.total_camera_sensor_area = []

    def get_security_history(self, security_state):
        """
        Returns a dummy version of the current security state to be used in the history.

        Parameters
        ----------
        security_state
            The current security state

        Returns
        -------
        history:
            A dummy version of the security state that can be saved to the history.
        """
        history = [self.get_dummies(security_state[i]) for i in range(8)]
        return history

    def update(self, security_state):
        """
        Updates the security history by adding a dummy security state to it that can be recorded without affecting the
        current state.
        Parameters
        ----------
        security_state
            The current observed security state.
        """
        self.sensor_state_history.append(self.get_security_history(security_state))
        if len(self.sensor_state_history) == 3:
            self.sensor_state_history.pop(0)

    @staticmethod
    def get_dummies(component_array):
        """
        Returns a dummy version of the provided component array, that can be safely stored.
        Parameters
        ----------
        component_array
            The array of components that needs to be saved.
        Returns
        -------
        copy:list
            A copy of the component array only containing dummy components.
        """
        copy = [[0 for x in range(len(component_array))] for y in range(len(component_array))]
        for x in range(len(component_array)):
            for y in range(len(component_array[x])):
                if component_array[x][y]:
                    copy[x][y] = component_array[x][y].get_dummy()
        return copy
