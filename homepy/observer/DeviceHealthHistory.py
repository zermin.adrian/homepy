"""
History class to record all general device health states.
"""


class SmartHomeHistory:
    def __init__(self):
        """
        Constructor for the smart home history.
        """
        self.smart_home_state_history = []
        self.smart_home_reward = []

    def update(self, smart_home_state):
        """
        Updates the smart home history by recording the given state.
        Parameters
        ----------
        smart_home_state
            The state to be recorded.
        """
        self.smart_home_state_history.append(self.get_smart_home_history(smart_home_state))
        if len(self.smart_home_state_history) == 3:
            self.smart_home_state_history.pop(0)


    @staticmethod
    def get_smart_home_history(smart_home_state):
        """
        Returns a dummy version of the current device health state to be used in the history.

        Parameters
        ----------
        smart_home_state
            The current state.
        Returns
        -------
        historic_state:list
            A dummy version of the device health state that can be safely stored without affecting the current state.
        """
        return [[device.get_dummy() for device in smart_home_state[0]],
                [repairman.get_dummy() for repairman in smart_home_state[1]]]
