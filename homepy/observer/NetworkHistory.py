import copy


class NetworkHistory:

    def __init__(self):
        """
        Constructor for the network history.
        """
        self.routing_state_history = []
        self.routing_reward_history = []
        self.arrived_packages = []

    def update(self, router_state):
        """
        Updates the history with the current network state.
        Parameters
        ----------
        router_state
            State to be recorded.
        """
        self.routing_state_history.append(self.get_router_history(router_state))
        if len(router_state[3]):
            for package in router_state[3]:
                self.arrived_packages.append(package)
        if len(self.routing_state_history) == 3:
            self.routing_state_history.pop(0)

    @staticmethod
    def get_router_history(routing_state):
        """
        Returns a copy of the current routing state for saving in the history.

        Parameters
        ----------
        routing_state
            The current routing state.
        Returns
        -------
            A copy of the current routing state to be used as a historic state.
        """
        history = [copy.deepcopy(i) for i in routing_state]
        return history
