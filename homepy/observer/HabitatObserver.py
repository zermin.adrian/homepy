import collections
import simpy

from homepy.observer.Observer import Observer
from homepy.observer.HabitatHistory import HabitatHistory
"""
Observer class to observe the simulation and provide the observer with the current simulated environmental state.
"""


class HabitatObserver(Observer):
    def __init__(self, env, habitat, device_health_controller):
        """
        Constructor for the habitat observer.
        Parameters
        ----------
        env
            The SimPy environment of the simulation.
        habitat
            The habitat the simulated devices "live" in
        device_health_controller
            Top-level controller ensuring general device health
        """
        super(HabitatObserver, self).__init__(env, habitat)
        self.heaters = self.habitat.get_empty_map()
        self.coolers = self.habitat.get_empty_map()
        self.thermostats = self.habitat.get_empty_map()
        self.thermometers = self.habitat.get_empty_map()
        self.humidifiers = self.habitat.get_empty_map()
        self.dehumidifiers = self.habitat.get_empty_map()
        self.light_bulbs = self.habitat.get_empty_map()
        self.humidity_sensors = self.habitat.get_empty_map()
        self.windows = self.habitat.get_empty_map()
        self.luminosity_sensors = self.habitat.get_empty_map()
        self.shutters = self.habitat.get_empty_map()
        self.window_openers = self.habitat.get_empty_map()
        self.current_habitat_state = []

        self.history = HabitatHistory()
        self.process = simpy.Process(self.env, self.monitor())
        self.device_health_controller = device_health_controller

    def get_state(self):
        """
        Gets the current environmental state of the simulation.
        Returns
        -------
        state:list
            A map of habitat tuples part of the simulated smart home that affect the environmental conditions.

        """
        state = self.habitat.get_empty_map()
        HabitatTuple = collections.namedtuple("habitat_tuple", "heater cooler thermometer humidifier dehumidifier"
                                                               " humiditysensor lightbulb luminositysensor "
                                                               " window shutter windowopener")
        for x_coordinate in range(len(state)):
            for y_coordinate in range(len(state[x_coordinate])):
                state[x_coordinate][y_coordinate] = HabitatTuple(
                    heater=self.heaters[x_coordinate][y_coordinate],
                    cooler=self.coolers[x_coordinate][y_coordinate],
                    thermometer=self.thermometers[x_coordinate][y_coordinate],
                    humidifier=self.humidifiers[x_coordinate][y_coordinate],
                    dehumidifier=self.dehumidifiers[x_coordinate][y_coordinate],
                    humiditysensor=self.humidity_sensors[x_coordinate][y_coordinate],
                    lightbulb=self.light_bulbs[x_coordinate][y_coordinate],
                    luminositysensor=self.luminosity_sensors[x_coordinate][y_coordinate],
                    window=self.windows[x_coordinate][y_coordinate],
                    shutter=self.shutters[x_coordinate][y_coordinate],
                    windowopener=self.window_openers[x_coordinate][y_coordinate])
        rooms = [[location[:] for location in room.coordinates] for room in self.habitat.rooms]
        return [state, rooms]

    def monitor(self):
        """
        Process that monitors the current habitat state of the simulated smart home.
        """
        while True:
            self.current_habitat_state = self.get_state()
            self.device_health_controller.predict_habitat_state(self.current_habitat_state)
            self.history.update(self.current_habitat_state)
            yield self.env.timeout(1)
