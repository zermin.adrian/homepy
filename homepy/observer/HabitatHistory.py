import collections
"""
History class for recording the history of the habitat states the simulation went through.
"""


class HabitatHistory:
    def __init__(self):
        """
        Constructor for the history class.
        """
        self.room_temperatures = []
        self.room_luminosities = []
        self.room_humidities = []
        self.habitat_state_history = []

    def update(self, habitat_state):
        """
        Adds the habitat_state to the history, by copying it and adding a dummy object of all simulated objects to
        the history.
        Parameters
        ----------
        habitat_state:list
            The habitat state to be recorded.
        """
        self.habitat_state_history.append(self.get_habitat_history(habitat_state))
        if len(self.habitat_state_history) == 3:
            self.habitat_state_history.pop(0)

    def get_habitat_history(self, habitat_state):
        """
        Returns a version of the current state that can be serialized and recorded.
        Parameters
        ----------
        habitat_state:list
            The habitat state to be recorded.

        Returns
        -------
        historic_state:list
            A copy of the habitat state.

        """
        state_copy = \
            [[self.get_dummy_tuple(habitat_tuple) for habitat_tuple in tuple_array] for tuple_array in habitat_state[0]]
        return [state_copy, habitat_state[1]]

    def get_dummy_tuple(self, habitat_tuple):
        """
        Returns a dummy version of a state tuple that doesnt affect the currently used state.
        Parameters
        ----------
        habitat_tuple
            The tuple to be copied.

        Returns
        -------
        historic_tuple:collections.namedtuple
            A dummy version of the original habitat tuple.

        """
        HabitatTuple = collections.namedtuple("habitat_tuple", "heater cooler thermometer humidifier dehumidifier"
                                                               " humiditysensor lightbulb luminositysensor "
                                                               " window shutter windowopener")
        useful_list = [0 for i in range(11)]
        for i in range(len(habitat_tuple)):
            if habitat_tuple[i]:
                useful_list[i] = habitat_tuple[i].get_dummy()
        historic_tuple = HabitatTuple._make(useful_list)    # Create tuple from list
        return historic_tuple
