import simpy
from homepy.observer.Observer import Observer
from homepy.observer.SecurityHistory import SecurityHistory

"""
Observer class that provides the controller with the current security state of the system.
"""


class SecurityObserver(Observer):
    def __init__(self, env, habitat, device_health_controller):
        """
        Constructor for the security state observer.

        Parameters
        ----------
        env
            The SimPy environment the simulation runs in.
        habitat
            The habitat the simulated devices "live" in.
        device_health_controller
            Top-level controller ensuring general device health.
        """
        super(SecurityObserver, self).__init__(env, habitat)
        self.alarms = self.habitat.get_empty_map()
        self.dislocators = self.habitat.get_empty_map()
        self.motors = self.habitat.get_empty_map()
        self.rotators = self.habitat.get_empty_map()
        self.cameras = self.habitat.get_empty_map()
        self.motionSensors = self.habitat.get_empty_map()
        self.doors = self.habitat.get_empty_map()
        self.controls = self.habitat.get_empty_map()
        self.situation_update_event = simpy.events.Event(self.env)
        self.current_security_state = []
        self.history = SecurityHistory()
        self.process = simpy.Process(self.env, self.monitor())
        self.device_health_controller = device_health_controller

    def get_state(self):
        """
        Returns the current observed security state
        """
        state = [self.motionSensors, self.cameras, self.dislocators, self.motors,
                  self.rotators, self.doors, self.controls, self.alarms]
        return state

    def monitor(self):
        """
        Monitor process to continuously update the current observed security state.
        """
        while True:
            self.current_security_state = self.get_state()
            self.device_health_controller.predict_security_state(self.current_security_state)
            self.history.update(self.current_security_state)
            yield self.env.timeout(1)
