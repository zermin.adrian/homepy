import simpy
from homepy.observer.Observer import Observer
from homepy.observer.DeviceHealthHistory import SmartHomeHistory
"""
Observer class that provides the controller with the current device health state of the system.
"""


class DeviceHealthObserver(Observer):
    def __init__(self, env, habitat):
        """
        Constructor for the device health observer.

        Parameters
        ----------
        env
            The SimPy environment the simulation runs in.
        habitat
            Top-level controller ensuring general device health.
        """
        super(DeviceHealthObserver, self).__init__(env, habitat)
        self.devices = []
        self.repairmen = []
        self.current_state = []
        self.history = SmartHomeHistory()
        self.process = simpy.Process(self.env, self.monitor())

    def monitor(self):
        """
        Monitor process to continuously update the current observed device health state.
        """
        while True:
            self.current_state = [self.devices, self.repairmen]
            self.history.update(self.current_state)
            yield self.env.timeout(1)
            print(self.env.now)
