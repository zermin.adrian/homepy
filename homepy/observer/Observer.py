class Observer:
    def __init__(self, env, habitat):
        """
        Constructor for the base class of all observers
        Parameters
        ----------
        env
            The SimPy environment the simulation runs in.
        habitat
            The habitat the simulated devices "live" in.
        """
        self.env = env
        self.habitat = habitat
        self.state = []
        self.mapsize = habitat.map_size

    def get_state(self):
        """
        Returns the current observed state
        """
        pass

    def monitor(self):
        """
        Monitor process to continuously update the current observed state.
        """
        pass
