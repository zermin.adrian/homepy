import simpy
import sys

from homepy.observer.Observer import Observer
from homepy.observer.NetworkHistory import NetworkHistory
"""
Observer class to observe the simulation and provide network state to the controller.
"""


class NetworkObserver(Observer):
    def __init__(self, env, habitat, device_health_controller):
        """
        Constructor for the network observer.
        Parameters
        ----------
        env
            The SimPy environment the simulation runs in.
        habitat
            The habitat the simulated devices "live" in.
        device_health_controller
            Top-level controller ensuring general device health.
        """
        super(NetworkObserver, self).__init__(env, habitat)
        self.env = env
        self.history = NetworkHistory()
        self.habitat = habitat

        self.gateways = []
        self.routers = []
        self.packages = []
        self.router_connections = []
        self.shortest_path_times = []
        self.connectables = []
        self.router_queues = []
        self.avg_throughput_times = []
        self.router_queues_hist = []
        self.current_routing_state = []
        self.throughput_times = []
        self.arrived_packages = []

        self.current_destinations = []
        self.error_packages = []
        self.signal_strengths = [[[0 for i in range(len(self.connectables))] for e in range(habitat.map_size)] for f in
                                 range(habitat.map_size)]
        self.process = simpy.Process(self.env, self.monitor())
        self.device_health_controller = device_health_controller

    def monitor(self):
        """
        Monitor process to continuously update the current observed network state.
        """
        while True:
            self.device_health_controller.predict_routing_state()
            self.update_connections()
            number_of_connectables = len(self.connectables)
            self.current_destinations = [sys.maxsize for i in range(number_of_connectables)]
            self.router_queues = []
            for connectable in self.connectables:
                self.router_queues.append(connectable.destination_queue)  # ToDo: Check whether this works as intended
                package = connectable.current_package
                if package:
                    self.current_destinations[connectable.connection_id] = package.destination.connection_id
            self.current_routing_state = self.get_state()
            self.history.update(self.current_routing_state)
            self.arrived_packages = []
            yield self.env.timeout(1)

    def get_state(self):
        """
        Provides the current network state of the simulation.
        """
        state = []
        state.append(self.router_connections)  # state = ([connection qualities between connectables]
        state.append(self.router_queues)  # [queue of destinations in each connectable, where client.queue.size = 0],
        state.append(self.current_destinations)  # [destinations of current routing packages in each connectable],
        state.append([package.get_dummy() for package in self.arrived_packages])     # [Packages arrived at this point in time])
        return state

    def update_connections(self):
        """
        Updates the current device connections.
        """
        self.router_connections = \
            [[0 for j in range(len(self.connectables))] for i in range(len(self.connectables))]
        for connectable in self.connectables:
            signal_strengths = self.signal_strengths[connectable.location[0]][connectable.location[1]]
            # check available signals at connectables' location
            for index in range(len(signal_strengths)):
                if signal_strengths[index] != 0:
                    self.router_connections[connectable.connection_id][index] = signal_strengths[index]
