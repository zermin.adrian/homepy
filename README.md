homepy
======


HomePy is a process-based discrete-event samrt home simulation
framework. It is based on the SimPy discrete event-simulation
framework and is intended as a test-bed for predictive maintenance
and adaptivity appliances in the smart home context


HTML documentation can be found in the docs folder and was generated with
Sphinx

Special thanks go to Markus Goerlich-Bucher for his assistance 
during the process of this Bachelors' thesis.